extends Node2D

signal level_saved

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().paused = true
	
	$"../CameraTarget/Camera2D/CanvasLayer/Control/Panel5".connect("gui_input", self, "inputEvent")
	
	if(get_tree().root.get_node("Data").currentEditorState != 1):
		$"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers".visible = false
		get_tree().paused = false
		
		$"../CameraTarget/Camera2D".current = true
		
		queue_free()
	else:
		$EditorMusic.play()
		get_tree().root.get_node("Data").freecam = true
		$"../CameraTarget/Camera2D/CanvasLayer/Control/NextLevel".visible = false
		
		$"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers/Control/TopBar/HBoxContainer/FileButton".get_popup().connect("id_pressed", self, "fileMenuUsed")
		
		# drawer shit
		for tab in $"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers/Control/TabContainer".get_children():
			tab.connect("mouse_entered", self, "openDrawer")
			tab.connect("mouse_exited", self, "closeDrawer")
	pass # Replace with function body.

func saveLevel():
	yield(get_tree(), "idle_frame")
	
	var packed_scene = PackedScene.new()
	packed_scene.pack(get_tree().get_current_scene())
	ResourceSaver.save(get_tree().get_current_scene().get_filename(), packed_scene)
	
	emit_signal("level_saved")
	get_tree().root.get_node("Data").pillMsgQueue.append("Level saved!")

func fileMenuUsed(idx):
	saveLevel()
	yield(self, "level_saved")
	match idx:
		0:
			get_tree().root.get_node("Data").currentEditorState = 2
			get_tree().root.get_node("Data").freecam = false
			get_tree().reload_current_scene()
		2:
			get_tree().root.get_node("Data").currentEditorState = 0
			get_tree().root.get_node("Data").freecam = false
			get_tree().paused = false
			get_tree().change_scene("res://Scenes/MainMenu.tscn")

func openDrawer():
	$Drawer.remove_all()
	$Drawer.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers/Control/TabContainer", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers/Control/TabContainer".rect_position, Vector2(0,380), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Drawer.start()

func closeDrawer():
	$Drawer.remove_all()
	$Drawer.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers/Control/TabContainer", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers/Control/TabContainer".rect_position, Vector2(0,540), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Drawer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func inputEvent(event):
	if Input.is_mouse_button_pressed(1) and $"../CameraTarget/Camera2D/CanvasLayer/Control/EditorLayers/Control/TabContainer/Tilemap".visible:
		var cell_pos = $"../TileMap".world_to_map(get_global_mouse_position())
		if(Input.is_physical_key_pressed(KEY_F)):
			$"../TileMap".set_cellv(cell_pos, 0)
		if(Input.is_physical_key_pressed(KEY_C)):
			$"../TileMap".set_cellv(cell_pos, -1)
