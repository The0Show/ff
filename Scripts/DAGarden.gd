extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var isBeingDragged = false

# REPLACE SOURCES FOR ANY SONGS YOU CHANGE
onready var songList = [
	{
		"name": "Main Menu",
		"source": "Hyped! 1 - Learn to Fly 3",
		"stream": get_tree().root.get_node("Data").getMusic("MainMenu"),
		"state": get_tree().root.get_node("Data").getMusicState("MainMenu")
	},
	{
		"name": "Learning the Concept",
		"source": "Main Theme 1 - Learn to Fly 3",
		"stream": get_tree().root.get_node("Data").getMusic("Idle"),
		"state": get_tree().root.get_node("Data").getMusicState("Idle")
	},
	{
		"name": "Experimenting",
		"source": "Main Theme 2 - Learn to Fly 3",
		"stream": get_tree().root.get_node("Data").getMusic("Retry"),
		"state": get_tree().root.get_node("Data").getMusicState("Retry")
	},
	{
		"name": "Airborne",
		"source": "Main Theme 3 - Learn to Fly 3",
		"stream": get_tree().root.get_node("Data").getMusic("Action"),
		"state": get_tree().root.get_node("Data").getMusicState("Action")
	},
	{
		"name": "Learned Some Physics",
		"source": "Space Rock Medley - Learn to Fly 3",
		"stream": get_tree().root.get_node("Data").getMusic("Credits"),
		"state": get_tree().root.get_node("Data").getMusicState("Credits")
	},
	{
		"name": "This Isn't a Race",
		"source": "Competition - Sonic Mania",
		"stream": get_tree().root.get_node("Data").getMusic("Competition"),
		"state": get_tree().root.get_node("Data").getMusicState("Competition")
	},
	{
		"name": "Furious with Friends",
		"source": "Multiplayer Lobby - Beat Saber",
		"stream": get_tree().root.get_node("Data").getMusic("Lobby"),
		"state": get_tree().root.get_node("Data").getMusicState("Lobby")
	},
	{
		"name": "Flapping Furiously",
		"source": "Hexagon Force - Waterflame",
		"stream": get_tree().root.get_node("Data").getMusic("Flappy"),
		"state": get_tree().root.get_node("Data").getMusicState("Flappy")
	},
	{
		"name": "Enter the Mainframe",
		"source": "Encore Save Select - Sonic Mania",
		"stream": get_tree().root.get_node("Data").getMusic("Haxxor"),
		"state": get_tree().root.get_node("Data").getMusicState("Haxxor")
	},
	{
		"name": "Unused Main Menu",
		"source": "Main Menu - Angry Birds",
		"stream": get_tree().root.get_node("Data").getMusic("MainMenu_OG"),
		"state": get_tree().root.get_node("Data").getMusicState("MainMenu_OG")
	}
]

var currentSong = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Tween.interpolate_property($AudioStreamPlayer, "volume_db", -80, 0, 0.35, Tween.TRANS_QUART, Tween.EASE_OUT)
	$Tween.interpolate_property($Camera2D/Control/CanvasLayer/ColorRect, "self_modulate", Color(1,1,1,1), Color(1,1,1,0), 0.5)
	
	$Tween.start()
	
	updateMetadata()
	
	$Sprite/Sprite2.set_texture(get_tree().root.get_node("Data").getTexture("Chuck"))
	$Sprite/Big.set_texture(get_tree().root.get_node("Data").getTexture("Big"))
	$Sprite/Big/Pig.set_texture(get_tree().root.get_node("Data").getTexture("Pig"))
	$Sprite/Birdbrain.set_texture(get_tree().root.get_node("Data").getTexture("Birdbrain"))
	pass # Replace with function body.

func updateMetadata():
	$Camera2D/Control/CanvasLayer/Label.set_text(str(currentSong + 1) + ". " + songList[currentSong].name)
	
	if(songList[currentSong].state):
		$Camera2D/Control/CanvasLayer/Label/Label.set_text("Modified by an external music pack")
	else:
		$Camera2D/Control/CanvasLayer/Label/Label.set_text(songList[currentSong].source)
	
	$AudioStreamPlayer.set_stream(songList[currentSong].stream)
	$Camera2D/Control/CanvasLayer/Label/HSlider.set_max(songList[currentSong].stream.get_length())
	
	$AudioStreamPlayer.play()
	
	$Camera2D/Control/CanvasLayer/Label/PrevSong.set_disabled(currentSong == 0)
	$Camera2D/Control/CanvasLayer/Label/NextSong.set_disabled(songList.size() == currentSong + 1)
	
	if(currentSong == 0):
		$Camera2D/Control/CanvasLayer/Label/PrevSong.set_default_cursor_shape(Control.CURSOR_FORBIDDEN)
	else:
		$Camera2D/Control/CanvasLayer/Label/PrevSong.set_default_cursor_shape(Control.CURSOR_POINTING_HAND)
	
	if(songList.size() == currentSong + 1):
		$Camera2D/Control/CanvasLayer/Label/NextSong.set_default_cursor_shape(Control.CURSOR_FORBIDDEN)
	else:
		$Camera2D/Control/CanvasLayer/Label/NextSong.set_default_cursor_shape(Control.CURSOR_POINTING_HAND)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (!isBeingDragged): $Camera2D/Control/CanvasLayer/Label/HSlider.set_value($AudioStreamPlayer.get_playback_position())
	
	$Sprite.position.x = (((OS.get_window_size().x/2)-get_viewport().get_mouse_position().x)/OS.get_window_size().x/2)*50
	$Sprite.position.y = (((OS.get_window_size().y/2)-get_viewport().get_mouse_position().y)/OS.get_window_size().y/2)*50
	
	$AudioStreamPlayer.set_pitch_scale(Engine.time_scale)
	pass


func _on_HSlider_drag_started():
	isBeingDragged = true
	pass # Replace with function body.


func _on_HSlider_drag_ended(value_changed):
	$AudioStreamPlayer.seek($Camera2D/Control/CanvasLayer/Label/HSlider.value)
	isBeingDragged = false
	pass # Replace with function body.


func _on_NextSong_pressed():
	currentSong += 1
	updateMetadata()
	pass # Replace with function body.


func _on_PrevSong_pressed():
	currentSong -= 1
	updateMetadata()
	pass # Replace with function body.


func _on_Button_pressed():
	isBeingDragged = true
	get_tree().root.get_node("Data").fromDA = true
	
	$Tween.remove_all()
	
	$AudioStreamPlayer.set_stream(load("res://Assets/SoundFX/SpecialWarp.wav"))
	$AudioStreamPlayer.play()
	
	$Tween.interpolate_property($Camera2D/Control/CanvasLayer/ColorRect, "self_modulate", Color(1,1,1,0), Color(1,1,1,1), 0.6)
	$Tween.start()
	
	yield($Tween, "tween_all_completed")
	
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
	pass # Replace with function body.
