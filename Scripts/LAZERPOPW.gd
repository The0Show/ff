extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var s = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("RESET")
	
	if(name != "LAZERPOPW"):
		translation.x = clamp($"../Bird".translation.x + rand_range(-5,5), -9, 9)
		
		visible = true
		$AnimationPlayer.play("New Anim")
		$AudioStreamPlayer.play()
		
		yield($AnimationPlayer, "animation_finished")
		queue_free()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(s):
		$"../Camera"/Tween.remove_all()
		$"../Camera"/Tween.interpolate_property($"../Camera", "cameraShake", Vector2(0.2,0.2), Vector2(0,0), 1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$"../Camera"/Tween.start()
		
		for body in $Area.get_overlapping_bodies():
			if(body is RigidBody):
				body.apply_impulse(Vector3(0,0,0), Vector3((body.translation.x-translation.x)*10,30,0))
		s = false
	pass
