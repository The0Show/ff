extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if(name != "Spatial"):
		visible = true
		$Tween.interpolate_property($Label3D,"translation",Vector3(0,0,0),Vector3(0,1,0),1)
		$Tween.interpolate_property($Label3D,"modulate",Color(1,1,1,1),Color(1,1,1,0),1)
		$Tween.interpolate_property($Label3D/Label3D2,"modulate",Color(0,0,0,1),Color(0,0,0,0),1)
		$Tween.start()
		
		yield($Tween, "tween_all_completed")
		queue_free()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
