extends Camera


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var cameraOffset = Vector2(0,0)
export var cameraBounds = Vector2(100,3)
export(Array, NodePath) var cameraTargets = []
export var cameraShake = Vector2(0,0)
export var shakeTimerr = 0.01
var shakeTimer = 0

onready var basePosition = Vector3(translation.x,translation.y,translation.z)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	shakeTimer -= delta
	if(shakeTimer <= 0):
		h_offset = rand_range(-cameraShake.x, cameraShake.x)
		v_offset = rand_range(-cameraShake.y, cameraShake.y)
		shakeTimer = shakeTimerr
	
	var combinedTranslation = Vector3(0,0,0)
	var distance = 0
	
	for target in cameraTargets:
		if(get_node(target).visible):
			combinedTranslation.x += get_node(target).translation.x
			combinedTranslation.y += get_node(target).translation.y
			
			if(get_node(target).translation.distance_to(Vector3(0,0,0)) > distance):
				distance = get_node(target).translation.distance_to(Vector3(0,0,0))
	
	
	
	translation.x = clamp((combinedTranslation.x/cameraTargets.size())+cameraOffset.x,basePosition.x-cameraBounds.x,basePosition.x+cameraBounds.x)
	translation.y = clamp((combinedTranslation.y/cameraTargets.size())+cameraOffset.y,basePosition.y-cameraBounds.y,basePosition.y+cameraBounds.y)
	
	translation.z = basePosition.z + clamp(lerp(-2,5,distance/10),-2,5)
	pass


func _input(event):
	if(event is InputEventKey and event.scancode == KEY_F4):
		$"..".health = 115
		$"..".updateBossHealthBar()
	if(event is InputEventKey and event.scancode == KEY_F2):
		fov = 90
