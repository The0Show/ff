extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var lines = [
	"Hacking into Rovio Entertainment Headquarters...\n............\nPassword: password\nAccess Granted",
	"I had a good job until my boss accused me of stealing!\nI better call Saul!",
	"This game is brought to you by: BEANS\nTry new fresh quality BEANS at [REDACTED]",
	"This game is brought to you by: NUTS\nTry fresh quality nuts at nuts.com!",
	"Will you marry me? Y/Y",
	"Checking the weather forecast...........",
	"..................loading.................",
	"If you can read this, you can read",
	"Someone kept telling me to learn some physics, I guess this is my destiny now",
	"AMOG",
	"US",
	"*crashes*",
	"null",
	"Brainwashing the CPU...",
	"Furious Fletchlings: Minimum System Requirements\n- Intel i5-4460 | AMD Ryzen 3 1200 CPU\n- 8 TB RAM\n- NVIDIA GTX 770 | AMD Radeon RX 570 GPU\n- 700 TB available space",
	"Restricted\nYour admin has blocked this page.",
	"        ",
	"Would you like to download more RAM?",
	"KRIS, DON'T YOU WANT TO BE A [[Big Shot]]?",
	"Furret Walk!",
	"you look nice c:",
	"You know the rules, and so do I.........................\nSay goodbye!",
	"Vamos a colegio!",
	"Istanbul was Constantinople\nNow it's Instanbul, not Constantinople",
	"[[Hyperlink Blocked]]",
	"Hexagons are the bestagons",
	"Viva Viva, Octagon!",
	"I wanna be the very worst, that no one ever was",
	"Reese's Puffs, Reese's Puffs, eat 'em up, eat 'em up, eat 'em up, eat 'em up!",
	"change da world\nmy final message. Goodb ye",
	"What color is an orange?",
	"Rodney - the Battleship that was involved in the Battle of Bismarck",
	"Please rise for the national anthem of gaming........................\n*halo theme starts playing*",
	"Who wrote these anyways?",
	"Who's line is it anyways?",
	"Mysteries of Life: With Tim and Moby",
	"Why are there even 40 of these?",
	"The longest possible FF run is 5.390663e+300 years.",
	"Beyblade, beyblade, let it rip!\n(please tell me someone else remembers this)",
	"ltf3 best flash game",
	"run 3 best flash game",
	"ya know, i'm working on things other than this game?\nthe0show.com/projects",
	"subscribe to the0show or im taking your RAM",
	"Top 1 Best Ways to Get Carpal Tunnel\n1. Beat Saber",
	"youtu.be/oW43WSO8fiU",
	"[rainbow freq=0.2 sat=10 val=20]haha l get rainbowed",
	"[/rainbow]thats enough of that",
	"adam stop getting 9 second runs"
]

var usedLines = []

var lineInt = -1
var prevLineInt = lineInt
var letterInt = 0
var line = ""

var vsyncPass = true

var cooldown = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	line = "GodOS\nBIOS version " + Engine.get_version_info().string + "\nLogging into user....................\nLogged in."
	
	for lined in lines:
		usedLines.clear()
		usedLines.append(lined)
	
	$SmoothScrollContainer/VBoxContainer/LaunchMulti.set_text(str(get_tree().root.get_node("Data").launchMulti))
	$SmoothScrollContainer/VBoxContainer/PigResist.set_text(str(get_tree().root.get_node("Data").pigResist))
	$SmoothScrollContainer/VBoxContainer/FrameLimiter.set_text(str(Engine.get_target_fps()))
	$SmoothScrollContainer/VBoxContainer/Gravity.set_text(str(Physics2DServer.area_get_param(get_viewport().find_world_2d().get_space(), Physics2DServer.AREA_PARAM_GRAVITY)))
	$SmoothScrollContainer/VBoxContainer/TimeSclae.set_text(str(Engine.time_scale))
	$SmoothScrollContainer/VBoxContainer/Speedrun.set_pressed(get_tree().root.get_node("Data").freecam)
	vsyncPass = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	cooldown -= delta
	
	if(cooldown <= 0):
		cooldown = 0.06
		
		if(letterInt < line.length()):
			$RichTextLabel.bbcode_text += line[letterInt]
			letterInt += 1
		else:
			$RichTextLabel.bbcode_text += "\n\n"
			
			lineInt = randi() % usedLines.size()
			letterInt = 0
			
			line = usedLines[lineInt]
			usedLines.remove(lineInt)
			
			if(usedLines.size() <= 0):
				usedLines.clear()
				for lined in lines:
					usedLines.append(lined)
		
		$RichTextLabel.bbcode_text = $RichTextLabel.bbcode_text.replace("_", "")
		$RichTextLabel.bbcode_text += "_"
	pass


func _on_HaxxorScreen_visibility_changed():
	$RichTextLabel.bbcode_text = ""
	line = "GodOS\nBIOS version " + Engine.get_version_info().string + "\nLogging into user....................\nLogged in."
	
	lineInt = -1
	prevLineInt = lineInt
	letterInt = 0
	
	usedLines.empty()
	
	for lined in lines:
		usedLines.clear()
		usedLines.append(lined)
	pass # Replace with function body.


func _on_LaunchMulti_text_changed(new_text):
	get_tree().root.get_node("Data").launchMulti = new_text.to_float()
	pass # Replace with function body.


func _on_Button_pressed():
	set_visible(false)
	$"../MainScreen".set_visible(true)
	pass # Replace with function body.


func _on_PigResist_text_changed(new_text):
	get_tree().root.get_node("Data").pigResist = new_text.to_float()
	pass # Replace with function body.


func _on_FrameLimiter_text_changed(new_text):
	if(!vsyncPass):
		OS.set_use_vsync(false)
	Engine.set_target_fps(new_text.to_int())
	pass # Replace with function body.


func _on_Gravity_text_changed(new_text):
	Physics2DServer.area_set_param(get_viewport().find_world_2d().get_space(), Physics2DServer.AREA_PARAM_GRAVITY, new_text.to_float())
	pass # Replace with function body.


func _on_TimeSclae_text_changed(new_text):
	Engine.set_time_scale(clamp(new_text.to_float(), 0.01, 9223372036854775807))
	pass # Replace with function body.


func _on_Speedrun_toggled(button_pressed):
	get_tree().root.get_node("Data").freecam = button_pressed
	pass # Replace with function body.
