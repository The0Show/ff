extends CanvasLayer

var GodotErrors = [
	"OK",
	"Failed",
	"Unavailable",
	"Unconfigured",
	"Unauthorized",
	"Parameter range error",
	"Out of memory",
	"File not found",
	"Bad drive",
	"Bad file path",
	"No permission",
	"File already in use",
	"Can't open file",
	"Can't write file",
	"Can't read file",
	"Unrecognized file",
	"File is corrupt",
	"Missing dependencies",
	"End of file",
	"Can't open",
	"Can't create",
	"Query failed",
	"Already in use",
	"Locked",
	"Timeout",
	"Can't connect",
	"Can't resolve",
	"Connection error",
	"Can't acquire resource",
	"Can't fork process",
	"Invalid data",
	"Invalid parameter",
	"Already exists",
	"Does not exist",
	"Can't read database",
	"Can't write database",
	"Compilation failed",
	"Method not found",
	"Linking failed",
	"Script failed",
	"Cycling link error",
	"Invalid declaration",
	"Duplicate symbol",
	"Parse error",
	"Busy error",
	"Skip error",
	"Help error",
	"Bug error",
	"Printer on fire."
]

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var file = File.new()
var filteredPacks = [
	"Flappy",
	"BigBall",
	"Boss"
]

var swProcessing = false

# Called when the node enters the scene tree for the first time.
func _ready():
	SilentWolf.Auth.connect("sw_login_succeeded", self, "onLoggedIn")
	SilentWolf.Auth.connect("sw_login_failed", self, "onLoginFailed")
	SilentWolf.Auth.connect("sw_registration_succeeded", self, "onLoggedIn")
	SilentWolf.Auth.connect("sw_registration_failed", self, "onLoginFailed")
	
	refreshLoginStatus()
	
	rebuildLevelSelect()
	rebuildPackSelect()
	
	if(OS.get_name() == "HTML5"):
		$ButtonLayout/QuitButton.set_disabled(true)
		$ButtonLayout/QuitButton.hint_tooltip = "This button doesn't work on this platform. Stop being lazy and close the tab instead."
		$ButtonLayout/QuitButton.mouse_default_cursor_shape = Control.CURSOR_FORBIDDEN
	
	$ButtonLayout/Speedrun.set_pressed(get_tree().root.get_node("Data").speedrun)
	
	get_tree().connect("files_dropped", self, "handlePckFiles")
	pass # Replace with function body.


func _on_OptionsButton_pressed():
	set_visible(false)
	$"../SettingsScren".set_visible(true)
	pass # Replace with function body.


func _on_QuitButton_pressed():
	get_tree().root.get_node("Data").isClosing = true
	get_tree().quit()
	pass # Replace with function body.


func _on_PlayButton_pressed():
	AudioServer.set_bus_volume_db(4,-80)
	AudioServer.set_bus_volume_db(5,-80)
	AudioServer.set_bus_volume_db(6,-80)
	
	get_tree().root.get_node("Data").timer = 0
	
	var err = get_tree().change_scene("res://Scenes/Levels/"+ get_tree().root.get_node("Data").currentLevelPack +"/1.tscn")
	
	if(err != OK):
		OS.alert("Scene change failed (" + str(err) + ")")
		get_tree().quit()
	pass # Replace with function body.


func _on_CreditsButton_pressed():
	set_visible(false)
	$"../CreditsScreen".set_visible(true)
	pass # Replace with function body.


func _on_Button2_pressed():
	if($"../SettingsScren".warning == 1):
		$"../SettingsScren/Button2/AudioStreamPlayer".play(0)
		
		var dir = Directory.new()
	
		if(file.file_exists("user://lvlsel")):
			dir.remove("user://lvlsel")
			
		$ButtonLayout/LevelSelect.set_visible(false)
		$ButtonLayout/Speedrun.set_visible(false)
		get_tree().root.get_node("Data").speedrun = false
		
		$"../SettingsScren".warning = 0
		$"../SettingsScren".updateDanger()
		
		get_tree().root.get_node("Data").achivesData = {"bigMistake": false, "firstTime": false, "goodTime": false, "itsNotThatBad": false, "sub30": false, "creative": false, "how": false, "rick": false, "hacker": false, "noclip": false, "pig-1": false, "perfection": false}
		get_tree().root.get_node("Data")._storeAchivements()
		
		SilentWolf.Auth.logout_player()
		
		yield(get_tree().create_timer(1),"timeout")
		
		AudioServer.set_bus_mute(1,true)
		OS.alert("Data cleared. The game will now restart.")
		
		JavaScript.eval("window.location.reload()")
		get_tree().quit()
	else:
		$"../SettingsScren".warning = 1
		$"../SettingsScren".updateDanger()
		
		$"../SettingsScren/Button2/Warning".play()
	pass # Replace with function body.


func _on_Speedrun_toggled(enabled):
	get_tree().root.get_node("Data").speedrun = enabled
	
	$ButtonLayout/LevelSelect.set_visible(!enabled)
	$ButtonLayout/LeaderboardButton.set_visible(enabled)
	if(enabled):
		if(!$"../CreditsScreen".visible):
			$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Competition"))
			$"../AudioStreamPlayer".play()
	else:
		if(!$"../CreditsScreen".visible):
			if(get_tree().root.get_node("Data").haxxor):
				$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Haxxor"))
				$"../AudioStreamPlayer".play()
			else:
				$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("MainMenu"))
				$"../AudioStreamPlayer".play()
	pass # Replace with function body.


#func _on_ChangelogButton_pressed():
#	if(OS.get_name() == "HTML5"):
#		JavaScript.eval('window.open("https://docs.google.com/document/d/1VnISeG7P0H5_UXn8SYDFRNktcykO8P6vMPZ1msu006I/edit?usp=sharing");')
#	else:
#		OS.shell_open("https://docs.google.com/document/d/1VnISeG7P0H5_UXn8SYDFRNktcykO8P6vMPZ1msu006I/edit?usp=sharing")
#	pass # Replace with function body.

func _process(_delta):
	$TextureRect.self_modulate = get_tree().root.get_node("Data").logoColor
	
	var doesLvlselExist = file.file_exists("user://lvlsel")
	
	$ButtonLayout/HackMode.set_visible(get_tree().root.get_node("Data").haxxor)
	$ButtonLayout/Speedrun.set_visible((!get_tree().root.get_node("Data").haxxor) and doesLvlselExist)
	$ButtonLayout/BattleButton.set_visible((!get_tree().root.get_node("Data").haxxor) and doesLvlselExist)
	
	$AuthButton/Panel/HBoxContainer/LoginButton.set_disabled($AuthButton/Panel/VBoxContainer/LineEdit.text == "" or $AuthButton/Panel/VBoxContainer/LineEdit2.text == "" or swProcessing)
	$AuthButton/Panel/HBoxContainer/SignupButton.set_disabled($AuthButton/Panel/VBoxContainer/LineEdit.text == "" or $AuthButton/Panel/VBoxContainer/LineEdit2.text == "" or swProcessing)
	
	$ButtonLayout/BattleButton.set_disabled(get_tree().root.get_node("Data").username == "0")
	if(get_tree().root.get_node("Data").username == "0"):
		$ButtonLayout/BattleButton.hint_tooltip = "Disconnected from game server\nRefresh to attempt a reconnect"
	else:
		$ButtonLayout/BattleButton.hint_tooltip = ""
	if(get_tree().root.get_node("Data").haxxor):
		$ButtonLayout/Speedrun.set_pressed(false)
	
	if(SilentWolf.Auth.logged_in_player == null and get_tree().root.get_node("Data").speedrun):
		$AuthButton.self_modulate = Color.yellow
	else:
		$AuthButton.self_modulate = Color.white

func rebuildLevelSelect():
	$ButtonLayout/LevelSelect.get_popup().clear()
	
	var dir = Directory.new()
	if dir.open("res://Scenes/Levels/" + get_tree().root.get_node("Data").currentLevelPack + "/") == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir():
				$ButtonLayout/LevelSelect.get_popup().add_item("Level " + file_name.replace(".tscn",""), int(file_name.replace(".tscn","")))
			file_name = dir.get_next()

func rebuildPackSelect():
	$ButtonLayout/LevelPack.get_popup().clear()
	
	var dir = Directory.new()
	if dir.open("res://Scenes/Levels/") == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir() and !filteredPacks.has(file_name):
				$ButtonLayout/LevelPack.get_popup().add_item(file_name)
			file_name = dir.get_next()

func refreshLoginStatus():
	if(SilentWolf.Auth.logged_in_player != null):
		$AuthButton.text = "Logged in as " + SilentWolf.Auth.logged_in_player + " (click to logout)"
	else:
		$AuthButton.text = "Not logged in (click to login)"
	

func _on_HackMode_pressed():
	set_visible(false)
	$"../HaxxorScreen".set_visible(true)
	pass # Replace with function body.


func _on_AchivesButtobn_pressed():
	pass # Replace with function body.


func _on_OptionsButton2_pressed():
	set_visible(false)
	$"../Achive".set_visible(true)
	pass # Replace with function body.

const CHUNK_SIZE = 1024
var dir = Directory.new()

func handlePckFiles(files, screen):
	var sec = Time.get_unix_time_from_system()
	
	var packIndex = {}
	
	var filepath = files[0]
	print(filepath)
	
	var err = file.open(filepath, File.READ)
	
	if(err != 0):
		get_tree().root.get_node("Data").pillMsgQueue.append("GDScript Error: " + GodotErrors[err])
	else:
		file.seek(0)
		
		var magik = file.get_buffer(4).get_string_from_utf8()
		
		if(magik == "GDPC"):
			file.close()
			
			if(!dir.dir_exists("user://Packages/")): dir.make_dir("user://Packages/")
			
			dir.copy(filepath, "user://Packages/" + filepath.replace("\\", "/").split("/")[-1])
			
			get_tree().root.get_node("Data").pillMsgQueue.append(filepath.replace("\\", "/").split("/")[-1] + " imported!")
			get_tree().root.get_node("Data").reimportLevels()
			rebuildPackSelect()
		elif(magik == "GCPF"):
			get_tree().root.get_node("Data").pillMsgQueue.append("FFMOD files are no longer supported.")
		else:
			pass
	
	file.close()


func _on_BattleButton_pressed():
	set_visible(false)
	$"../BattleModeScreen".set_visible(true)
	pass # Replace with function body.


func _on_Button_button_down():
	$TextureRect/Timer.start(4)
	pass # Replace with function body.


func _on_Button_button_up():
	$TextureRect/Timer.stop()
	pass # Replace with function body.


func _on_Timer_timeout():
	get_tree().root.get_node("Data")._unlockEverything()
	pass # Replace with function body.


func _on_AuthButton_pressed():
	if(SilentWolf.Auth.logged_in_player == null):
		$AuthButton/Panel.set_visible(!$AuthButton/Panel.visible)
	else:
		SilentWolf.Auth.logout_player()
		refreshLoginStatus()
	pass # Replace with function body.


func _on_LevelEditorButton_pressed():
	set_visible(false)
	$"../LevelEditor".set_visible(true)
	pass # Replace with function body.


func _on_MainScreen_visibility_changed():
	$AuthButton/Panel.set_visible(false)
	pass # Replace with function body.


func _on_LoginButton_pressed():
	SilentWolf.Auth.login_player($AuthButton/Panel/VBoxContainer/LineEdit.text, $AuthButton/Panel/VBoxContainer/LineEdit2.text, true)
	swProcessing = true
	pass # Replace with function body.


func _on_Panel_visibility_changed():
	$AuthButton/Panel/VBoxContainer/LineEdit.text = ""
	$AuthButton/Panel/VBoxContainer/LineEdit2.text = ""
	pass # Replace with function body.

func onLoggedIn():
	$AuthButton/Panel.visible = false
	swProcessing = false
	refreshLoginStatus()

func onLoginFailed(error):
	swProcessing = false
	OS.alert(error)

func _on_SignupButton_pressed():
	swProcessing = true
	SilentWolf.Auth.register_player($AuthButton/Panel/VBoxContainer/LineEdit.text, null, $AuthButton/Panel/VBoxContainer/LineEdit2.text, $AuthButton/Panel/VBoxContainer/LineEdit2.text)
	pass # Replace with function body.


func _on_LeaderboardButton_pressed():
	$ColorRect.visible = true
	pass # Replace with function body.


func _on_Button_pressed():
	$ColorRect.visible = false
	pass # Replace with function body.


func _on_ColorRect_visibility_changed():
	$ColorRect/Panel2/Button.disabled = true
	
	for child in $ColorRect/Panel2/SmoothScrollContainer/VBoxContainer.get_children():
		if(child.name != "EntryBase"): child.queue_free()
	
	if($ColorRect.visible):
		$ColorRect/Panel2/Status.text = "Downloading..."
		
		yield(SilentWolf.Scores.get_high_scores(0, get_tree().root.get_node("Data").currentLevelPack + "@" + get_tree().root.get_node("Data").levelPackKeys[get_tree().root.get_node("Data").currentLevelPack]), "sw_scores_received")
		
		$ColorRect/Panel2/Button.disabled = false
		
		if(SilentWolf.Scores.scores.size() <= 0):
			$ColorRect/Panel2/Status.text = "Leaderboard is empty"
		else:
			$ColorRect/Panel2/Status.text = ""
			for pos in SilentWolf.Scores.scores.size():
				var entry = $ColorRect/Panel2/SmoothScrollContainer/VBoxContainer/EntryBase.duplicate()
				
				entry.get_child(0).text = str(pos+1) + "."
				entry.get_child(1).text = SilentWolf.Scores.scores[pos].player_name
				entry.get_child(2).text = str(stepify(65535-(SilentWolf.Scores.scores[pos].score/1000), 0.01)) + "s"
				
				if(SilentWolf.Scores.scores[pos].player_name == SilentWolf.Auth.logged_in_player):
					entry.get_child(0).add_color_override("font_color", Color(1,0.77,0))
					entry.get_child(1).add_color_override("font_color", Color(1,0.77,0))
					entry.get_child(2).add_color_override("font_color", Color(1,0.77,0))
				
				$ColorRect/Panel2/SmoothScrollContainer/VBoxContainer.add_child(entry)
				entry.visible = true
	pass # Replace with function body.
