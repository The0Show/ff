extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal join_room
signal player_list_change
signal start_game
signal end_game
signal goal_scored

export var openDebug: bool = OS.is_debug_build()
export var speedrun = false
export var timer = 0.0
export var newpb = false
export var sky = true
export var debugPos = Vector2(-69420,-69420)

export var haxxor = false

export var lineReadyColor = Color.green
export var lineColor = Color.lightblue
export var logoColor = Color(0.92,0,0,1)

export var launchMulti = 2
export var pigResist = 1

var achivesFile = File.new()

var _client = WebSocketClient.new()
export var websocket_url = "wss://learnandfocus.the0show.com:3000"
#export var websocket_url = "ws://127.0.0.1:3000"
var err

export var username = "0"

var currentScene = ""
var connected = false
var loggedIn = false

export var isClosing = false

export var achivesData = {"bigMistake": false, "firstTime": false, "goodTime": false, "itsNotThatBad": false, "sub30": false, "creative": false, "how": false, "rick": false, "hacker": false, "noclip": false, "pig-1": false, "perfection": false}

export var achivesInfo = {
	"bigMistake": {
		"name": "Big Mistake",
		"desc": "You don't know what you're getting yourself into...",
		"secret": false
	},
	"firstTime": {
		"name": "Physics Student",
		"desc": "Complete Furious Fletchlings.",
		"secret": false
	},
	"goodTime": {
		"name": "Newcomer",
		"desc": "Complete a speedrun.",
		"secret": false
	},
	"itsNotThatBad": {
		"name": "It's Not That Bad",
		"desc": "Get a time under 60 seconds.",
		"secret": false
	},
	"sub30": {
		"name": "Sonic Speed",
		"desc": "Get a time under 30 seconds.",
		"secret": false
	},
	"creative": {
		"name": "Skilled Runner",
		"desc": "Get a time under 15 seconds.",
		"secret": false
	},
	"how": {
		"name": "FURYØS!!!",
		"desc": "Get a time under 10 seconds.",
		"secret": false
	},
	"pig-1": {
		"name": "Wait, you weren't supposed to do that",
		"desc": "Hit the pig on Level 3.",
		"secret": true
	},
	"rick": {
		"name": "Never Gonna Give",
		"desc": "You're welcome.",
		"secret": true
	},
	"hacker": {
		"name": "Enter the Matrix",
		"desc": "[REDACTED]",
		"secret": true
	},
	"noclip": {
		"name": "Extravagant Adventures",
		"desc": "Bye-bye, ocean",
		"secret": true
	},
	"perfection": {
		"name": "Perfectionist",
		"desc": "You really spent the time to do all of these?",
		"secret": true
	}
}

export var fromDA = false
export var freecam = false

var musicIndex = {
	"Action": load("res://Assets/Music/Action.ogg"),
	"Competition": load("res://Assets/Music/Competition.ogg"),
	"Credits": load("res://Assets/Music/Credits.ogg"),
	"Haxxor": load("res://Assets/Music/Haxxor.ogg"),
	"Idle": load("res://Assets/Music/Idle.ogg"),
	"MainMenu": load("res://Assets/Music/MainMenu.ogg"),
	"MainMenu_OG": load("res://Assets/Music/MainMenu_OG.ogg"),
	"Retry": load("res://Assets/Music/Retry.ogg"),
	"Lobby": load("res://Assets/Music/Lobby.ogg"),
	"Flappy": load("res://Assets/Music/Flappy.ogg"),
	"TrueCredits": load("res://Assets/Music/TrueCredits.ogg")
}

var musicState = {
	"Action": false,
	"Competition": false,
	"Credits": false,
	"Haxxor": false,
	"Idle": false,
	"MainMenu": false,
	"MainMenu_OG": false,
	"Retry": false,
	"Lobby": false,
	"Flappy": false,
	"TrueCredits": false,
}

var spriteIndex = {
	"Sky": load("res://Assets/Sprites/Sky.png"),
	"Big": load("res://Assets/Sprites/Big.png"),
	"Birdbrain": load("res://Assets/Sprites/Birdbrain.png"),
	"Chuck": load("res://Assets/Sprites/Chuck.png"),
	"Pig": load("res://Assets/Sprites/Pig.png")
}

export var logoString = "Furious Fletchlings"

export var blockLevelSwitching = false

export var lobbyId = ""
export var playerData = {}
export var isHost = false
export var winningPlayer = {
	"playerName": "",
	"time": "0"
}
export var gameConfig = {
	"launchMulti": 2,
	"pigResist": 1,
	"gravity": 98,
	"gameMode": 0,
	"mapPack": 0,
	"ghostBirds": false,
	"goalsToWin": 15,
}
export var defaultGameConfig = {
	"launchMulti": 2,
	"pigResist": 1,
	"gravity": 98,
	"gameMode": 0,
	"mapPack": 0,
	"ghostBirds": false,
	"goalsToWin": 15,
}

export var ballPos = Vector2(0,0)
export var ballRot = 0

export(Texture) var birdTexture
export(Texture) var pigTexture

export var birdScale = Vector2(1.286,1.347)
export var pigScale = Vector2(0.7,0.7)

export var isFoolsOfApril = false

export var currentLevelPack = "Default"
export var levelPackKeys = {
	"Default": "3e230097a325057949797389480a742f32e87e48d5740d49074a2a5ec1911ef8"
}

enum EditorState {
	None,
	Editing,
	Playtesting
}

export(EditorState) var currentEditorState = 0

export var matchResults = []

export var currentHat = "default"

var adj = [
	"Furious",
	"Shiny",
	"Fast",
	"Aggravated",
	"Dramatic",
	"Ruling",
	"Sad",
	"Picky",
	"Titular",
	"Kidnapped",
	"Annoying",
	"Persistant",
	"Unfair",
	"Exploting",
	"Informal",
	"Rare"
]

var noun = [
	"Fletchling",
	"Bird",
	"LegalAction",
	"Aviator",
	"TwitterUser",
	"RedditMod",
	"Benadryl",
	"Error",
	"Wasp",
	"Engine",
	"Godot",
	"Stick",
	"Ruler",
	"Fledgling",
	"FileSystem",
	"Doru",
	"Jacob"
]

export(String) var playername = "Player" + str(OS.get_unix_time())

func rollUsername():
	playername = adj[rand_range(0, adj.size())] + "" + noun[rand_range(0,noun.size())]

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	rollUsername()
	
	if(get_tree().root.get_node_or_null("Data") != null):
		queue_free()
	
	if(achivesFile.file_exists("user://achivements")):
		achivesFile.open("user://achivements", File.READ_WRITE)
		achivesData = achivesFile.get_var(true)
		achivesFile.close()
	else:
		_storeAchivements()
	
	get_parent().call_deferred("remove_child", self)
	get_tree().root.call_deferred("add_child", self)
	
	# Connect base signals to get notified of connection open, close, and errors.
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	# This signal is emitted when not using the Multiplayer API every time
	# a full packet is received.
	# Alternatively, you could check get_peer(1).get_available_packets() in a loop.
	_client.connect("data_received", self, "_on_data")
	
	get_tree().connect("tree_changed", self, "_onTree")

	# Initiate connection to the given URL.
	err = _client.connect_to_url(websocket_url, ["lws-mirror-protocol"])
	if err != OK:
		print("Unable to connect")
	
	#Steam.Steam.getPlayerAvatar(2)
	#Steam.Steam.connect("avatar_loaded", self, "loaded_avatar")
	pass # Replace with function body.

#func loaded_avatar(id: int, size: int, buffer: PoolByteArray):
#	var AVATAR = Image.new()
#	var STEAM_AVATAR = ImageTexture.new()
#	AVATAR.create_from_data(size, size, false, Image.FORMAT_RGBA8, buffer)
#	STEAM_AVATAR.create_from_image(AVATAR)
#
#	loggedIn = true
#	playername = Steam.Steam.getPersonaName()
#
#	$CanvasLayer/Control/AchivementBox/Sprite.set_texture(STEAM_AVATAR)
#
#	$CanvasLayer/Control/AchivementBox/Label3.set_text("Logged in")
#	$CanvasLayer/Control/AchivementBox/Label.set_text("Logged in successfully as " + str(Steam.Steam.getPersonaName()))
#
#	$CanvasLayer/Control/AnimationPlayer.play("LoggedIn")

func _input(_event):
	if(Input.is_action_just_pressed("debug")):
		openDebug = !openDebug
	if(Input.is_action_just_pressed("fsbrow")):
		$CanvasLayer/FileDialog.access = FileDialog.ACCESS_USERDATA
		$CanvasLayer/FileDialog.current_dir = "user://Packages"
		$CanvasLayer/FileDialog.filters = PoolStringArray(["*.fflvlpck ; Level Packs"])
		$CanvasLayer/FileDialog.popup_centered()
		

func _closed(was_clean = false):
	# was_clean will tell you if the disconnection was correctly notified
	# by the remote peer before closing the socket.
	print("Closed, clean: ", was_clean)
	username = "0"
	if(lobbyId != ""):
		OS.alert("Server disconnect!")
		get_tree().change_scene("res://Scenes/MainMenu.tscn")
	
	lobbyId = ""
	gameConfig = defaultGameConfig

func _connected(proto = ""):
	connected = true
	# This is called on connection, "proto" will be the selected WebSocket
	# sub-protocol (which is optional)
	print("Connected with protocol: ", proto)

func array_to_string(arr: Array, splitter: String) -> String:
	var s = ""
	for i in arr:
		s += String(i)
		s += splitter
	return s.trim_suffix(splitter)

func _on_data():
	# Print the received packet, you MUST always use get_peer(1).get_packet
	# to receive data from server, and not get_packet directly when not
	# using the MultiplayerAPI.
	if(username == "0"):
		username = _client.get_peer(1).get_packet().get_string_from_utf8()
	else:
		var data = JSON.parse(_client.get_peer(1).get_packet().get_string_from_utf8()).result
		$CanvasLayer/Control/Label.set_text("")
		
		var command = data[0]
		
		data.remove(0)
		
		match command:
			"ghostBird":
				if(data[0] != username):
					if(data[1] == get_tree().current_scene.name):
						if(get_tree().current_scene.get_node_or_null(data[0]) == null):
							var newNode = get_tree().current_scene.get_node("FakeBird").duplicate()
							get_tree().current_scene.add_child(newNode)
							get_tree().current_scene.move_child(newNode, get_tree().current_scene.get_node("FakeBird").get_position_in_parent())
							
							newNode.name = data[0]
							newNode.get_node("Label").text = playerData[data[0]].name
							newNode.get_node("AnimatedSprite").play(playerData[data[0]].hat)
							
							newNode.set_visible(true)
						
						get_tree().current_scene.get_node(data[0]).position.x = data[2]
						get_tree().current_scene.get_node(data[0]).position.y = data[3]
						get_tree().current_scene.get_node(data[0]).rotation_degrees = data[4]
			"assignGhostBirdToTeam":
				if(data[0] != username):
					if(get_tree().current_scene.get_node_or_null(data[0]) == null):
						var newNode = get_tree().current_scene.get_node("FakeBird").duplicate()
						get_tree().current_scene.add_child(newNode)
						
						newNode.name = data[0]
						newNode.get_node("Label").text = playerData[data[0]].name
						
						newNode.set_visible(true)
						
					print(data[0])
					
					match str(data[1]):
						"0":
							get_tree().current_scene.get_node(data[0]).set_texture(birdTexture)
							get_tree().current_scene.get_node(data[0]).set_scale(birdScale)
							get_tree().current_scene.get_node(data[0]).get_child(0).set_position(Vector2(3.888,-128.434))
						"1":
							get_tree().current_scene.get_node(data[0]).set_texture(pigTexture)
							get_tree().current_scene.get_node(data[0]).set_scale(pigScale)
							get_tree().current_scene.get_node(data[0]).get_child(0).set_position(Vector2(-28,-145))
			"ballUpdate":
				ballPos = Vector2(data[0],data[1])
				ballRot = data[2]
			"goalScored":
				emit_signal("goal_scored", data[0], data[1], data[2])
			"gameConfigUpdate":
				gameConfig = data[0]
			"endGame":
				winningPlayer.playerName = data[0] 
				winningPlayer.time = data[1]
				matchResults = data[2]
				emit_signal("end_game") 
			"joinRoom":
				lobbyId = data[0]
				emit_signal("join_room")
			"doNotJoinRoom":
				pillMsgQueue.append(data[0])
				get_tree().change_scene("res://Scenes/MainMenu.tscn")
			"host":
				isHost = true
			"updatePlayerList":
				playerData = data[0]
				emit_signal("player_list_change")
			"startGame":
				emit_signal("start_game")
				
				pigResist = gameConfig.pigResist
				launchMulti = gameConfig.launchMulti
				Physics2DServer.area_set_param(get_viewport().find_world_2d().get_space(), Physics2DServer.AREA_PARAM_GRAVITY, gameConfig.gravity)
			"message":
				$CanvasLayer/Control/Label.set_text("")
			
				var text = []
				
				for i in array_to_string(data, " "):
					$CanvasLayer/Control/Label.text += i
					$CanvasLayer/Control/AudioStreamPlayer2.play()
					text.append(i)
					yield(get_tree().create_timer(0.06), "timeout")
				
				yield(get_tree().create_timer(5), "timeout")
				
				for i in text.size():
					text.remove(randi() % text.size())
					$CanvasLayer/Control/Label.text = array_to_string(text, "")
					yield(get_tree().create_timer(0.01), "timeout")
				
				$CanvasLayer/Control/Label.set_text("")
			
			"timescale":
				Engine.set_time_scale(data[0].to_float())
				
				if("Levels" in get_tree().current_scene.get_filename()):
					get_tree().current_scene.get_node("Bird").prevTimeScale = data[0].to_float()
					get_tree().current_scene.prevTimeScale = data[0].to_float()
			
			"sudo":
				if(data[0] == "shutdown"):
					JavaScript.eval("window.close();")
					JavaScript.eval('window.location.replace("https://google.com");')
					get_tree().quit()
			
			"rickroll":
				OS.shell_open("https://www.youtube.com/watch?v=eBGIQ7ZuuiU")
			
			"identify":
				$CanvasLayer/Control/ColorRect.set_visible(true)
				
				yield(get_tree().create_timer(0.5), "timeout")
				
				$CanvasLayer/Control/ColorRect.set_visible(false)
			
			"data":
				match data[0]:
					"pigResist":
						pigResist = data[1].to_float()
					"launchMulti":
						launchMulti = data[1].to_float()
					"logoString":
						data.remove(0)
						logoString = array_to_string(data, " ")
					"blockLevelSwitching":
						blockLevelSwitching = data[1] == "true"
			
			"physics":
				match data[0]:
					"gravity":
						Physics2DServer.area_set_param(get_viewport().find_world_2d().space, Physics2DServer.AREA_PARAM_GRAVITY, data[1].to_float())
					"gravityDirection":
						Physics2DServer.area_set_param(get_viewport().find_world_2d().space, Physics2DServer.AREA_PARAM_GRAVITY_VECTOR, Vector2(data[1].to_float(),data[2].to_float()))
			
			"focusCamera":
				var inputEvent = InputEventAction.new()
				inputEvent.action = "free_camera"
				inputEvent.pressed = true
				inputEvent.strength = 1
				
				Input.parse_input_event(inputEvent)
				yield(get_tree().create_timer(0.1), "timeout")
				inputEvent.pressed = false
				inputEvent.strength = 0
				Input.parse_input_event(inputEvent)
			
			"enviroment":
				$WorldEnvironment/Tween.remove_all()
				
				match data[0]:
					"adjustment_brightness":
						$WorldEnvironment/Tween.interpolate_property($WorldEnvironment.environment, "adjustment_brightness", $WorldEnvironment.environment.adjustment_brightness, data[1].to_float(), data[2].to_float(), Tween.TRANS_CUBIC)
						$WorldEnvironment/Tween.start()
					"adjustment_contrast":
						$WorldEnvironment/Tween.interpolate_property($WorldEnvironment.environment, "adjustment_contrast", $WorldEnvironment.environment.adjustment_contrast, data[1].to_float(), data[2].to_float(), Tween.TRANS_CUBIC)
						$WorldEnvironment/Tween.start()
					"adjustment_saturation":
						$WorldEnvironment/Tween.interpolate_property($WorldEnvironment.environment, "adjustment_saturation", $WorldEnvironment.environment.adjustment_saturation, data[1].to_float(), data[2].to_float(), Tween.TRANS_CUBIC)
						$WorldEnvironment/Tween.start()
					"glow_intensity":
						$WorldEnvironment/Tween.interpolate_property($WorldEnvironment.environment, "glow_intensity", $WorldEnvironment.environment.glow_intensity, data[1].to_float(), data[2].to_float(), Tween.TRANS_CUBIC)
						$WorldEnvironment/Tween.start()
					"glow_strength":
						$WorldEnvironment/Tween.interpolate_property($WorldEnvironment.environment, "glow_strength", $WorldEnvironment.environment.glow_strength, data[1].to_float(), data[2].to_float(), Tween.TRANS_CUBIC)
						$WorldEnvironment/Tween.start()
					"glow_bloom":
						$WorldEnvironment/Tween.interpolate_property($WorldEnvironment.environment, "glow_bloom", $WorldEnvironment.environment.glow_bloom, data[1].to_float(), data[2].to_float(), Tween.TRANS_CUBIC)
						$WorldEnvironment/Tween.start()

export var pillMsgQueue = []
var dir = Directory.new()

const CHUNK_SIZE = 1024

func hash_file(path):
	var ctx = HashingContext.new()
	var file = File.new()
	# Start a SHA-256 context.
	ctx.start(HashingContext.HASH_SHA256)
	# Check that file exists.
	if not file.file_exists(path):
		return
	# Open the file to hash.
	file.open(path, File.READ)
	# Update the context after reading each chunk.
	while not file.eof_reached():
		ctx.update(file.get_buffer(CHUNK_SIZE))
	# Get the computed hash.
	var res = ctx.finish()
	# Print the result as hex string and array.
	return res.hex_encode()

func reimportLevels():
	var stats = [0,0]
	
	if dir.open("user://Packages/") == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir() and file_name.find(".fflvlpck"):
				stats[0] += 1
				var success = ProjectSettings.load_resource_pack("user://Packages/" + file_name)
				if(success):
					stats[1] += 1
					var key = hash_file("user://Packages/" + file_name)
					levelPackKeys[file_name.replace(".fflvlpck", "")] = key
				else:
					pillMsgQueue.append("Level pack " + file_name + " failed to load.")
			file_name = dir.get_next()
			
		pillMsgQueue.append("Successfully imported " + str(stats[0]) + "/" + str(stats[1]) + " level packs.")

func _process(delta):
	# Call this in _process or _physics_process. Data transfer, and signals
	# emission will only happen when calling this function.
	_client.poll()
	
	OS.set_window_title(TranslationServer.translate(logoString))
	
	if(!$CanvasLayer/Control/AnimationPlayer2.is_playing() and pillMsgQueue.size() > 0):
		$CanvasLayer/Control/Label2.set_text(pillMsgQueue[0])
		$CanvasLayer/Control/AnimationPlayer2.play("PillMsg")
		pillMsgQueue.remove(0)
	
func _notification(what):
	if(what == NOTIFICATION_WM_QUIT_REQUEST):
		isClosing = true
		get_tree().quit()
		
func _onTree():
	if(!isClosing and get_tree().current_scene != null and get_tree().current_scene.name != currentScene and connected):
		_client.get_peer(1).put_packet(Marshalls.utf8_to_base64(JSON.print(["sceneChange", get_tree().current_scene.name])).to_utf8())
		currentScene = get_tree().current_scene.name
	elif(isClosing):
		_client.disconnect_from_host()

func _unlockAchivement(id, silent = false):
	if(!achivesData[id] and (!haxxor or id == "perfection")):
		achivesData[id] = true
		$CanvasLayer/Control/AchivementBox/Label3.set_text("Achivement Unlocked")
		$CanvasLayer/Control/AchivementBox/Label.set_text(achivesInfo[id].name)
		$CanvasLayer/Control/AchivementBox/Label2.set_text(achivesInfo[id].desc)
		
		if(!silent):
			$CanvasLayer/Control/AudioStreamPlayer.play()
			$CanvasLayer/Control/AnimationPlayer.stop()
			$CanvasLayer/Control/AnimationPlayer.play("New Anim")
		
		_storeAchivements()

func _unlockEverything():
	if(!achivesFile.file_exists("user://lvlsel")):
		achivesFile.open("user://lvlsel", File.WRITE)
		achivesFile.store_double(-69)
		
		achivesFile.close()
	
	$CanvasLayer/Control/AudioStreamPlayer3.play()
	
	for key in achivesData:
		_unlockAchivement(key, true)
	
	pillMsgQueue.append("Unlocked everything!")

func showCornerMessage(title, desc1, desc2):
	$CanvasLayer/Control/AchivementBox/Label3.set_text(title)
	$CanvasLayer/Control/AchivementBox/Label.set_text(desc1)
	$CanvasLayer/Control/AchivementBox/Label2.set_text(desc2)
	
	$CanvasLayer/Control/AnimationPlayer.stop()
	$CanvasLayer/Control/AnimationPlayer.play("New Anim")

func _storeAchivements():
	achivesFile.close()
	achivesFile.open("user://achivements", File.WRITE)
	
	achivesFile.store_var(achivesData)
	achivesFile.close()

func getMusic(id):
	return musicIndex[id]

func getMusicState(id):
	return musicState[id]
	
func getTexture(id):
	return spriteIndex[id]

func tellUserAboutModImport(name, sec):
	$CanvasLayer/Control/AchivementBox/Label3.set_text("Mod Imported")
	$CanvasLayer/Control/AchivementBox/Label.set_text(name)
	$CanvasLayer/Control/AchivementBox/Label2.set_text("Took " + str(Time.get_unix_time_from_system() - sec) + " seconds")
		
	$CanvasLayer/Control/AnimationPlayer.stop()
	$CanvasLayer/Control/AnimationPlayer.play("New Anim")

func tellUserAboutModImportFailure(name, reason):
	$CanvasLayer/Control/AchivementBox/Label3.set_text("Mod Import Failed")
	$CanvasLayer/Control/AchivementBox/Label.set_text(name)
	$CanvasLayer/Control/AchivementBox/Label2.set_text(reason)
		
	$CanvasLayer/Control/AnimationPlayer.stop()
	$CanvasLayer/Control/AnimationPlayer.play("New Anim")

func send_packet(packet):
	_client.get_peer(1).put_packet(packet)

func joinRoom(id):
	var pnts = playername
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "joinRoom", id, pnts, currentHat])).to_utf8())
	
func kickPlayer(id):
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "kickPlayer", id])).to_utf8())
	
func promotePlayer(id):
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "promotePlayer", id])).to_utf8())
	isHost = false
	
func updateHat():
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "updateHat", currentHat])).to_utf8())

func startGame():
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "startGame"])).to_utf8())
	
func sendTime():
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "finalTime", str(timer)])).to_utf8())
	
func sendGhostBird(position, rotation):
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "ghostBird", get_tree().current_scene.name, position.x, position.y, rotation])).to_utf8())
	
func sendTeam(team):
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "bigBall-setTeam", team])).to_utf8())
	
func sendBallHit(velocity, rotation):
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "bigBall-ballHit", velocity.x, velocity.y, rotation])).to_utf8())
	
func sendWallHit():
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "bigBall-wallHit"])).to_utf8())
	
func sendBallReset():
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "bigBall-ballReset"])).to_utf8())

func sendGoalScore(team):
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "bigBall-goalScored", team])).to_utf8())
	
func leaveGame():
	isHost = false
	lobbyId = ""
	matchResults = []
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "leaveRoom"])).to_utf8())
				
	pigResist = defaultGameConfig.pigResist
	launchMulti = defaultGameConfig.launchMulti
	
	gameConfig = {
		"launchMulti": defaultGameConfig.launchMulti,
		"pigResist": defaultGameConfig.pigResist,
		"gravity": defaultGameConfig.gravity,
		"gameMode": defaultGameConfig.gameMode,
		"mapPack": defaultGameConfig.mapPack,
		"ghostBirds": defaultGameConfig.ghostBirds,
		"goalsToWin": defaultGameConfig.goalsToWin,
	}
	
	Physics2DServer.area_set_param(get_viewport().find_world_2d().get_space(), Physics2DServer.AREA_PARAM_GRAVITY, defaultGameConfig.gravity)

func updateGameConfig(config):
	send_packet(Marshalls.utf8_to_base64(JSON.print(["multiplayer", "updateGameConfig", config])).to_utf8())

func _on_FileDialog_file_selected(path):
	if(path.ends_with(".fflvlpck")):
		dir.remove(ProjectSettings.globalize_path(path))
		$CanvasLayer/Control.mouse_filter =Control.MOUSE_FILTER_STOP
		pillMsgQueue.append("Please wait...")
		yield(get_tree().create_timer(5), "timeout")
		$CanvasLayer/Control.mouse_filter =Control.MOUSE_FILTER_IGNORE
		pillMsgQueue.append("Package will be deleted on next launch.")
		get_tree().reload_current_scene()
	pass # Replace with function body.
