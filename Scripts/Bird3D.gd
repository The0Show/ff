extends RigidBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var prevMousePos = Vector2(0,0)

export var prevTimeScale = Engine.time_scale

var timeScaleTween = Tween.new()

var freeze = false
var disable = false
var enableCamera = false

var limit = 1

func _ready():
	add_child(timeScaleTween)
	
	randomize()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(freeze and !disable):
		$PullSFX.set_volume_db(lerp(0, 10, clamp(abs((get_viewport().get_mouse_position()-prevMousePos).x)/100,0,1)))
		$"../Camera/Control".mouse_default_cursor_shape = Control.CURSOR_MOVE
	else:
		$PullSFX.set_volume_db(-80)
		$"../Camera/Control".mouse_default_cursor_shape = Control.CURSOR_ARROW
	
	if(!freeze):
		prevMousePos = get_viewport().get_mouse_position()
	pass
	
func vector2_to_vector3(vector):
	return Vector3(vector.x,-vector.y,0)
	
func vector2_to_clampedvector3(vector):
	return Vector3(clamp(vector.x,-20,20),-clamp(vector.y,-20,20),0)

func _input(event):
	if(visible):
		if(event is InputEventMouseMotion):
			if(!disable):
				pass
				#$"Line3D".end_point = vector2_to_clampedvector3((-(get_viewport().get_mouse_position()-prevMousePos)/10))
		
		if(event is InputEventMouseButton and !disable and event.button_index == 1 and get_tree().root.get_node("Data").timer >= 0):
			#$Line3D.visible = event.pressed
			if(!event.pressed and abs((get_viewport().get_mouse_position()-prevMousePos).x) > limit):
				linear_velocity = Vector3(0,0,0)
				linear_velocity = vector2_to_clampedvector3(-((get_viewport().get_mouse_position()-prevMousePos)*0.1))
				
				$LaunchSFX.pitch_scale = rand_range(0.9,1.15)
				$LaunchSFX.play()
				
				disable = false
				freeze = false
			elif(!event.pressed and abs((get_viewport().get_mouse_position()-prevMousePos).x) < limit):
				freeze = false
			else:
				freeze = true
		
		if(event is InputEventKey):
			if(get_tree().current_scene.name != "Flappy"):
				if(Input.is_action_just_pressed("slowmo") and disable):
					timeScaleTween.remove_all()
					
					timeScaleTween.interpolate_method(Engine, "set_time_scale", Engine.time_scale, 0.33, 0.1)
					timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel".rect_position, Vector2(-468,0), 0.2)
					timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2".rect_position, Vector2(-455,538), 0.2)
					
					timeScaleTween.start()
				elif(Input.is_action_just_released("slowmo") and disable):
					timeScaleTween.remove_all()
					
					timeScaleTween.interpolate_method(Engine, "set_time_scale", Engine.time_scale, prevTimeScale, 0.1)
					timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel".rect_position, Vector2(-468,-62), 0.1)
					timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2".rect_position, Vector2(-455,600), 0.1)
					timeScaleTween.start()

var count = 0
