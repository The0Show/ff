extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum BossPhases {
	IDLE,
	THROWING_BLOCKS,
	PINCH,
	LAZERPOPW,
	FREEZE
}

var switchablePhases = [
	BossPhases.THROWING_BLOCKS,
	BossPhases.LAZERPOPW
]

var phaseCooldowns = {
	BossPhases.THROWING_BLOCKS: 400,
	BossPhases.LAZERPOPW: 0,
}

export var currentBossState = BossPhases.IDLE
export var frametimer = 0
export var statetimer = 0
export var health = 1000
var stunmark = 0
var stunamt = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	
	$WorldEnvironment/AnimationPlayer.play("RESET")
	$AnimationPlayer.play("Intro")
	
	# margins margins we fucking love margins
	$Camera/Control/ProgressBar.set_anchors_and_margins_preset(Control.PRESET_CENTER_BOTTOM)
	$Camera/Control/ProgressBar.rect_position = Vector2(40,530)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	frametimer -= 1
	statetimer -= 1
	
	
	if($Camera/Control/ProgressBar2.visible):
		stunamt = 10
		stunmark = clamp(stunmark-0.001, $Camera/Control/ProgressBar2.min_value, $Camera/Control/ProgressBar2.max_value)
	else:
		stunmark = clamp(stunmark-0.0005, $Camera/Control/ProgressBar2.min_value, $Camera/Control/ProgressBar2.max_value)
		stunamt = stunmark
	
	$Camera/Control/ProgressBar2.value = stunmark
	if(!$Camera/Control/ProgressBar2.visible and stunmark == $Camera/Control/ProgressBar2.max_value):
		$Camera/Control/ProgressBar2.visible = true
		currentBossState = BossPhases.THROWING_BLOCKS
		get_tree().root.get_node("Data").pillMsgQueue.append("Trolløs is stunned and drops his defences")
	
	if($Camera/Control/ProgressBar2.visible and stunmark == $Camera/Control/ProgressBar2.min_value):
		$Camera/Control/ProgressBar2.visible = false
		get_tree().root.get_node("Data").pillMsgQueue.append("Trolløs recovers and stands his ground")
	
	$FDSpace/Armature/polygon58.rotate_y(0.0005)
	
	if(statetimer <= 0 and switchablePhases.has(currentBossState) and !$Camera/Control/ProgressBar2.visible):
		currentBossState = switchablePhases[rand_range(0, switchablePhases.size())]
		statetimer = phaseCooldowns[currentBossState]
		print("phase chance")
		
	match currentBossState:
		BossPhases.THROWING_BLOCKS:
			if(frametimer <= 0):
				var block = $BlockThrowable.duplicate()
				
				block.visible = true
				
				add_child(block)
				
				block.translation.x = 20
				
				var left = randf() >= 0.5
				
				if(left):
					block.translation.x *= -1
				
				block.translation.y = clamp($Bird.translation.y+($Bird.linear_velocity.y*0.1),-1,9999999)
				block.angular_velocity.z = rand_range(-10,10)
				if(left): block.linear_velocity.x = 25
				if(!left): block.linear_velocity.x = -25
				block.linear_velocity.y = 5
				frametimer = rand_range(40,75)
		BossPhases.PINCH:
			if(frametimer <= 0):
				var block = $BlockThrowable.duplicate()
				
				block.visible = true
				
				add_child(block)
				
				block.translation.x = 20
				
				var left = randf() >= 0.5
				
				if(left):
					block.translation.x *= -1
				
				block.translation.y = clamp($Bird.translation.y+($Bird.linear_velocity.y*0.1),-1,9999999)
				block.angular_velocity.z = rand_range(-10,10)
				if(left): block.linear_velocity.x = 50
				if(!left): block.linear_velocity.x = -50
				block.linear_velocity.y = 5
				
				frametimer = rand_range(38,60)
			
		BossPhases.LAZERPOPW:
			add_child($LAZERPOPW.duplicate(7))
			
			statetimer = phaseCooldowns[BossPhases.THROWING_BLOCKS]
			currentBossState = BossPhases.THROWING_BLOCKS
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	if(currentBossState != BossPhases.FREEZE):
		statetimer = phaseCooldowns[BossPhases.THROWING_BLOCKS]
		currentBossState = BossPhases.THROWING_BLOCKS
	pass # Replace with function body.


func _on_OutOfBounds_body_exited(body):
	if(body.name == "Bird"):
		if(currentBossState != BossPhases.FREEZE):
			if(currentBossState == BossPhases.PINCH):
				$WorldEnvironment/AnimationPlayer.play("New Anim (copy)")
			
			currentBossState = BossPhases.FREEZE
			
			body.visible = false
			$Position3D.translation = body.translation
			
			$Camera.cameraTargets[0] = "../Position3D"
			
			$OutOfBounds/Tween.interpolate_property($Position3D, "translation", $Position3D.translation, Vector3(0,0,0), 0.8, Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$OutOfBounds/Tween.start()
			
			$AnimationPlayer.play("GameOver")
			
			$Bird/PullSFX.stop()
			
			if(OS.is_debug_build()):
				get_tree().reload_current_scene()
			else:
				yield($AnimationPlayer,"animation_finished")
				get_tree().change_scene("res://Scenes/Levels/Boss/Continue.tscn")
	else:
		body.queue_free()
	pass # Replace with function body.

func vec3_to_vec2(vec3):
	return Vector2(vec3.x, vec3.y)

func _on_AttackZone_body_entered(body):
	if(body.name != "Bird" and currentBossState != BossPhases.IDLE and currentBossState != BossPhases.FREEZE):
		var velr = abs((body.linear_velocity.x+body.linear_velocity.y)/2)
		var velav = clamp(round(velr*stunamt), 1, 100)
		
		print("attacking with " + body.name)
		var tween = Tween.new()
		body.add_child(tween)
		
		var attackPoint = Vector3(rand_range(-3,3),rand_range(-3,3),-14)
		
		tween.interpolate_property(body, "translation", body.translation, attackPoint, 1)
		tween.interpolate_property(body, "scale", body.scale, Vector3(0,0,0), 1)
		tween.start()
		
		yield(tween, "tween_completed")
		tween.queue_free()
		$Camera/Tween.remove_all()
		$Camera/Tween.interpolate_property($Camera, "cameraShake", Vector2(0.1,0.1), Vector2(0,0), 1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$Camera/Tween.start()
		
		var hittext = $Spatial.duplicate(7)
		add_child(hittext)
		hittext.translation = body.translation
		hittext.get_child(0).text = str(velav)
		hittext.get_child(0).get_child(0).text = str(velav)
		
		$Camera/Control/AnimationPlayer.stop(true)
		$Camera/Control/AnimationPlayer.play("New Anim")
		
		body.queue_free()
		
		if(currentBossState != BossPhases.FREEZE):
			$AudioStreamPlayer3D.play()
		
			health -= velav
			stunmark += velr/48
			updateBossHealthBar()
	pass # Replace with function body.

var oop = false
func updateBossHealthBar():
	$Camera/Control/ProgressBar/Tween.remove_all()
	$Camera/Control/ProgressBar/Tween.interpolate_property($Camera/Control/ProgressBar, "value", $Camera/Control/ProgressBar.value, health, 1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Camera/Control/ProgressBar/Tween.start()
	
	if(health <= 80 and !oop):
		$WorldEnvironment/AnimationPlayer.play("New Anim")
		oop = true
	
	if(health <= 0):
		currentBossState = BossPhases.FREEZE
		
		statetimer = 1000000
		frametimer = 1000000
		
		$AnimationPlayer.play("Ending")
		$WorldEnvironment/AnimationPlayer.play("New Anim (copy) (copy)")
		
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene("res://Scenes/MainMenu.tscn")

func _process(delta):
	$AudioStreamPlayer.pitch_scale = lerp(1,1.07,smoothstep(900,980,clamp(1000-health, 900, 980)))
