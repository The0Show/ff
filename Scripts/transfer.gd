extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_LineEdit_text_entered(new_text):
	SilentWolf.Scores.persist_score(SilentWolf.Auth.logged_in_player, int((65535*1000)-float(new_text)*1000), get_tree().root.get_node("Data").currentLevelPack + "@" + get_tree().root.get_node("Data").levelPackKeys[get_tree().root.get_node("Data").currentLevelPack])
	pass # Replace with function body.
