extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var prevSpeedrunSetting = false
var disableBypass = true

# Called when the node enters the scene tree for the first time.
func _ready():
	$LineEdit2.text = get_tree().root.get_node("Data").playername
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$TextureRect.self_modulate = get_tree().root.get_node("Data").logoColor
	$TextureRect.set_text(get_tree().root.get_node("Data").logoString)
	
	if(disableBypass):
		#$PlayButton.set_disabled($LineEdit2.text == "")
		$PlayButton.set_disabled(true)
		$PlayButton2.set_disabled($LineEdit2.text == "" or $LineEdit.text == "")
	pass


func _on_Button_pressed():
	set_visible(false)
	$"../MainScreen".set_visible(true)
	pass # Replace with function body.


func _on_BattleModeScreen_visibility_changed():
	if(visible):
		if(!get_tree().root.get_node("Data").speedrun):
			$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Competition"))
			$"../AudioStreamPlayer".play()
		prevSpeedrunSetting = get_tree().root.get_node("Data").speedrun
		get_tree().root.get_node("Data").speedrun = true
	else:
		get_tree().root.get_node("Data").speedrun = prevSpeedrunSetting
	
		if(!$"../CreditsScreen".visible):
			if(get_tree().root.get_node("Data").haxxor):
				$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Haxxor"))
				$"../AudioStreamPlayer".play()
			elif(!get_tree().root.get_node("Data").speedrun):
				$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("MainMenu"))
				$"../AudioStreamPlayer".play()
	pass # Replace with function body.


func _on_PlayButton_pressed():
	disableBypass = false
	
	$Button.set_disabled(true)
	$PlayButton2.set_disabled(true)
	$LineEdit.set_editable(false)
	$LineEdit2.set_editable(false)
	$PlayButton.set_disabled(true)
	
	$PlayButton.set_text("Finding match...")
	$PlayButton.set_default_cursor_shape(Control.CURSOR_WAIT)
	pass # Replace with function body.


func _on_PlayButton2_pressed():
	disableBypass = false
	
	$Button.set_disabled(true)
	$PlayButton2.set_disabled(true)
	$LineEdit.set_editable(false)
	$LineEdit2.set_editable(false)
	$PlayButton.set_disabled(true)
	
	$PlayButton2.set_text("Joining lobby...")
	$PlayButton2.set_default_cursor_shape(Control.CURSOR_WAIT)
	
	get_tree().root.get_node("Data").joinRoom($LineEdit.text)
	get_tree().root.get_node("Data").connect("join_room", self, "joinRoom")
	pass # Replace with function body.

func joinRoom():
	get_tree().change_scene("res://Scenes/Lobby.tscn")


func _on_LineEdit2_focus_entered():
	#Steam.Steam.showFloatingGamepadTextInput(0,0,0,50,50)
	pass # Replace with function body.


func _on_LineEdit_focus_entered():
	#Steam.Steam.showFloatingGamepadTextInput(0,0,0,50,50)
	pass # Replace with function body.


func _on_Button45_pressed():
	get_tree().root.get_node("Data").rollUsername()
	$LineEdit2.text = get_tree().root.get_node("Data").playername
	pass # Replace with function body.


func _on_LineEdit2_text_changed(new_text):
	get_tree().root.get_node("Data").playername = new_text
	pass # Replace with function body.
