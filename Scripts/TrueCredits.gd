extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AudioStreamPlayer.set_stream(get_tree().root.get_node("Data").musicIndex["TrueCredits"])
	$AnimationPlayer.play("New Anim")
	$TextureRect.set_text(get_tree().root.get_node("Data").logoString)
	$TextureRect.self_modulate = get_tree().root.get_node("Data").logoColor
	
	# margins
	$TextureRect.set_anchors_and_margins_preset(Control.PRESET_HCENTER_WIDE)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(abs(($AnimationPlayer.current_animation_position-2) - $AudioStreamPlayer.get_playback_position()) > 0.2 and $AudioStreamPlayer.playing):
		$AnimationPlayer.seek($AudioStreamPlayer.get_playback_position()+2)
	pass


func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
	pass # Replace with function body.
