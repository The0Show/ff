extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum MusicState {
	IDLE,
	RETRY,
	ACTION
}

enum Character {
	Big,
	Hubba
}

export(Character) var character = 1

var prevMousePos = Vector2(0,0)

export var prevTimeScale = Engine.time_scale

var timeScaleTween = Tween.new()

var freeze = false
var disable = false
var enableCamera = false

var limit = 100

func _ready():
	if(get_tree().root.get_node("Data").lobbyId != ""): $AnimatedSprite.play(get_tree().root.get_node("Data").currentHat)
	
	if(character == 0):
		$Sprite.set_texture(get_tree().root.get_node("Data").getTexture("Big"))
	else:
		$Sprite.set_texture(get_tree().root.get_node("Data").getTexture("Chuck"))
	
	add_child(timeScaleTween)
	
	randomize()
	
	var startPos = rand_range(0, $BGM1.get_stream().get_length())
	
	$BGM1.set_stream(get_tree().root.get_node("Data").getMusic("Idle"))
	$BGM3.set_stream(get_tree().root.get_node("Data").getMusic("Action"))
	
	if($"../".name != "Flappy"):
		$BGM2.set_stream(get_tree().root.get_node("Data").getMusic("Retry"))
	else:
		$BGM2.set_stream(get_tree().root.get_node("Data").getMusic("Flappy"))
		startPos = 0
	
	$BGM1.play(startPos)
	$BGM2.play(startPos)
	
	if(get_tree().root.get_node("Data").lobbyId != "" and get_tree().root.get_child_count() <= 3 and (get_tree().current_scene.name == "1" or get_tree().current_scene.name == "Flappy")):
		get_node("../CameraTarget/Camera2D/CanvasLayer/Control/AnimationPlayer").play("New Anim")
	
	if(get_tree().root.get_node("Data").lobbyId == ""):
		$BGM1.volume_db = 0
		get_node("../CameraTarget/Camera2D/CanvasLayer/Control/Panel3").set_visible(false)
		if(get_tree().root.get_child_count() > 3 or get_tree().root.get_node("Data").speedrun):
			changeMusicState(MusicState.RETRY)
		else:
			changeMusicState(MusicState.IDLE)
	else:
		if(get_tree().root.get_child_count() <= 3 and (get_tree().current_scene.name == "1" or get_tree().current_scene.name == "Flappy")):
			while(get_node("../CameraTarget/Camera2D/CanvasLayer/Control/AnimationPlayer").current_animation_position < 2.5):
				yield(get_tree(), "idle_frame")
		else:
			get_node("../CameraTarget/Camera2D/CanvasLayer/Control/Panel3").set_visible(false)
		
		changeMusicState(MusicState.RETRY)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(get_tree().current_scene.name == "BigBall"):
		enableCamera = true
		limit = -1
	
	if(get_tree().current_scene.name == "Pacer"):
		disable = $"../AudioStreamPlayer".get_playback_position() < 39.5
		$"../CameraTarget/Camera2D/CanvasLayer/Control/ProgressBar".value = $"../AudioStreamPlayer".get_playback_position()
		$"../CameraTarget/Camera2D/CanvasLayer/Control/ProgressBar/Label".set_text(str(clamp(($"../AudioStreamPlayer".get_playback_position()-39.5)/(1362.443-39.5), 0, 1)*100)+"%")
		
	if(!disable):
		$"../Line2D".set_position(position)
		if(!freeze):
			$"../Line2D".set_visible(false)
		elif(abs((get_viewport().get_mouse_position()-prevMousePos).x) > limit):
			$"../Line2D".set_default_color(get_tree().root.get_node("Data").lineReadyColor)
			$"../Line2D".set_visible(true)
		else:
			$"../Line2D".set_default_color(get_tree().root.get_node("Data").lineColor)
			$"../Line2D".set_visible(true)
	
	if(freeze and !disable):
		$PullSFX.set_volume_db(lerp(0, 10, clamp(abs((get_viewport().get_mouse_position()-prevMousePos).x)/100,0,1)))
		$"../CameraTarget/Camera2D/CanvasLayer/Control".mouse_default_cursor_shape = Control.CURSOR_MOVE
	else:
		$PullSFX.set_volume_db(-80)
		$"../CameraTarget/Camera2D/CanvasLayer/Control".mouse_default_cursor_shape = Control.CURSOR_ARROW
	
	if(!freeze):
		prevMousePos = get_viewport().get_mouse_position()
	
	if(enableCamera):
		if(get_tree().current_scene.name != "Flappy"):
			if(get_tree().current_scene.name == "BigBall"):
				$"../CameraTarget".set_position((position+$"../Ball".position)/2)
				$"../CameraTarget/Camera2D".zoom.x = clamp(position.distance_to($"../Ball".position)/500,2,3.5)
				$"../CameraTarget/Camera2D".zoom.y = clamp(position.distance_to($"../Ball".position)/500,2,3.5)
			else:
				$"../CameraTarget".set_position(position)
		
	if(position.y > 586):
		get_tree().root.get_node("Data")._unlockAchivement("noclip")
	pass

func setBGM1Db(value: float):
	var index = AudioServer.get_bus_index("BGM1")
	AudioServer.set_bus_volume_db(index, value)

func setBGM2Db(value: float):
	var index = AudioServer.get_bus_index("BGM2")
	AudioServer.set_bus_volume_db(index, value)

func setBGM3Db(value: float):
	var index = AudioServer.get_bus_index("BGM3")
	AudioServer.set_bus_volume_db(index, value)

func changeMusicState(value: int):
	$Tween.remove_all()
	
	var currentDb = [AudioServer.get_bus_volume_db(4), AudioServer.get_bus_volume_db(5), AudioServer.get_bus_volume_db(6)]
	
	if(value == MusicState.IDLE):
		$Tween.interpolate_method(self, "setBGM1Db", currentDb[0], 0, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.interpolate_method(self, "setBGM2Db", currentDb[1], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.interpolate_method(self, "setBGM3Db", currentDb[2], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
	elif(value == MusicState.ACTION):
		$BGM3.play()
		$Tween.interpolate_method(self, "setBGM1Db", currentDb[0], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.interpolate_method(self, "setBGM2Db", currentDb[1], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.interpolate_method(self, "setBGM3Db", currentDb[2], 0, 0.1, Tween.TRANS_QUART, Tween.EASE_OUT)
	elif(value == MusicState.RETRY):
		$Tween.interpolate_method(self, "setBGM1Db", currentDb[0], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.interpolate_method(self, "setBGM2Db", currentDb[1], 0, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.interpolate_method(self, "setBGM3Db", currentDb[2], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
	
	$Tween.start()

func _input(event):
	if(event is InputEventMouseMotion):
		if(!disable):
			$"../Line2D".set_points([Vector2(0,0), -(get_viewport().get_mouse_position()-prevMousePos)])
	
	if(event is InputEventMouseButton and !disable and event.button_index == 1 and get_tree().root.get_node("Data").timer >= 0):
		if(get_tree().current_scene.name == "Flappy"):
			linear_velocity = Vector2(0,-380)
		else:
			if(!event.pressed and abs((get_viewport().get_mouse_position()-prevMousePos).x) > limit):
				disable = true
				enableCamera = true
				
				linear_velocity = Vector2(0,0)
				linear_velocity = -((get_viewport().get_mouse_position()-prevMousePos)*get_tree().root.get_node("Data").launchMulti)
				
				$"../CameraTarget/Camera2D/CanvasLayer/Control/Debug".birdVel = linear_velocity
				
				if(get_tree().current_scene.name != "BigBall"): changeMusicState(MusicState.ACTION)
				
				get_parent().state = 1
				
				$LaunchSFX.pitch_scale = rand_range(0.9,1.15)
				$LaunchSFX.play()
				
				for child in get_parent().get_node("Level").get_children():
					if("Pig" in child.get_name()):
						child.canBeKilled = true
				
				if(get_tree().current_scene.name == "BigBall" or get_tree().current_scene.name == "Pacer"):
					disable = false
					freeze = false
			elif(!event.pressed and abs((get_viewport().get_mouse_position()-prevMousePos).x) < limit):
				freeze = false
			else:
				freeze = true
	
	if(event is InputEventKey):
		if(get_tree().current_scene.name != "Flappy"):
			if(Input.is_action_just_pressed("slowmo") and (disable or get_tree().current_scene.name == "Pacer")):
				timeScaleTween.remove_all()
				
				timeScaleTween.interpolate_method(Engine, "set_time_scale", Engine.time_scale, 0.33, 0.1)
				timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel".rect_position, Vector2(-468,0), 0.2)
				timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2".rect_position, Vector2(-455,538), 0.2)
				
				timeScaleTween.start()
			elif(Input.is_action_just_released("slowmo") and (disable or get_tree().current_scene.name == "Pacer")):
				timeScaleTween.remove_all()
				
				timeScaleTween.interpolate_method(Engine, "set_time_scale", Engine.time_scale, prevTimeScale, 0.1)
				timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel".rect_position, Vector2(-468,-62), 0.1)
				timeScaleTween.interpolate_property($"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2", "rect_position", $"../CameraTarget/Camera2D/CanvasLayer/Control/Panel2".rect_position, Vector2(-455,600), 0.1)
				timeScaleTween.start()

var count = 0

func _physics_process(delta):
	if(count == 0):
		count += 1
	else:
		if(get_tree().root.get_node("Data").gameConfig.ghostBirds):
			get_tree().root.get_node("Data").sendGhostBird(position, rotation_degrees)
		count = 0
