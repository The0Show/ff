extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("New Anim")
	
	# godot keeps fucking with my margins! yay!
	$Camera2D/CanvasLayer/Control/Label.set_anchors_and_margins_preset(Control.PRESET_BOTTOM_WIDE)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if(event is InputEventKey and event.scancode == KEY_ENTER):
		$AnimationPlayer.seek(31.5)

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene("res://Scenes/Levels/Boss/1.tscn")
	pass # Replace with function body.
