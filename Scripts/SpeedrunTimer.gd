extends Label

export var freeze = false

var personalBest = -69

func _ready():
	if(!get_tree().root.get_node("Data").speedrun and !freeze):
		queue_free()
	
	var file = File.new()
	if(file.file_exists("user://lvlsel")):
		file.open("user://lvlsel", File.READ_WRITE)
		
		var double = file.get_double()
		
		if(file.get_as_text() == "yes, indeed they have" or double == 0):
			file.close()
			
			file.open("user://lvlsel", File.WRITE)
			
			file.store_double(-69)
			personalBest = -69
		else:
			personalBest = double
		
		file.close()
	
	if(get_tree().root.get_node("Data").newpb):
		get_tree().root.get_node("Data").newpb = false
		
		$AudioStreamPlayer.play()
		$AnimationPlayer.play("NewPB")
		
		yield($AudioStreamPlayer, "finished")
		
		$AnimationPlayer.stop()
		
		$PersonalBest.visible = true

func _process(delta):
	set_text(str(stepify(get_tree().root.get_node("Data").timer, 0.01)))
	$PersonalBest.set_text("PB: " + str(stepify(personalBest, 0.01)))
	
	if(personalBest == -69):
		$PersonalBest.set_text("PB: None, yet...")
	
	if(!freeze):
		get_tree().root.get_node("Data").timer += delta
		
		if(get_tree().root.get_node("Data").timer > personalBest and personalBest != -69):
			$PersonalBest.set_self_modulate(Color.red)
	else:
		set_visible(get_tree().root.get_node("Data").speedrun)
	pass
