extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().root.get_node("Data").timer = 0
	$ColorRect.mouse_filter = Control.MOUSE_FILTER_STOP
	$Label.set_anchors_and_margins_preset(Control.PRESET_CENTER_TOP)
	$Label.rect_position = Vector2(398.5,96)
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	$AnimationPlayer.play("RESET")
	$Sprite3D.rotate(rand_range(-360,360))
	$AnimationPlayer.play("New Anim")
	yield($AnimationPlayer,"animation_finished")
	$ColorRect.mouse_filter = Control.MOUSE_FILTER_IGNORE
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Sprite3D.rotate(delta)
	pass


func _on_Button_pressed():
	$ColorRect.mouse_filter = Control.MOUSE_FILTER_STOP
	$AnimationPlayer.play("Yes")
	yield($AnimationPlayer,"animation_finished")
	
	get_tree().change_scene("res://Scenes/Levels/Boss/1.tscn")
	pass # Replace with function body.


func _on_Button2_pressed():
	$ColorRect.mouse_filter = Control.MOUSE_FILTER_STOP
	$AnimationPlayer.play("No")
	yield($AnimationPlayer,"animation_finished")
	
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
	pass # Replace with function body.
