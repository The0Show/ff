extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var warningAudio = [load("res://Assets/SoundFX/Delete1.wav"), load("res://Assets/SoundFX/Delete2.wav")]
var warningText = ["Clear Data", "Are you sure?"]

export var warning = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/GlobalVol.set_value(AudioServer.get_bus_volume_db(1))
	$VBoxContainer/MusicVol.set_value(AudioServer.get_bus_volume_db(2))
	$VBoxContainer/SoundFXVol.set_value(AudioServer.get_bus_volume_db(3))
	
	$Sky.pressed = get_tree().root.get_node("Data").sky
	$VBoxContainer2/LogoColor.color = get_tree().root.get_node("Data").logoColor
	$VBoxContainer2/LineColor.color = get_tree().root.get_node("Data").lineColor
	$VBoxContainer2/LineReadyColor.color = get_tree().root.get_node("Data").lineReadyColor
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func updateDanger():
	$Button2.set_text(warningText[warning])
	$Button2/Warning.set_stream(warningAudio[warning])
	pass


func _on_GlobalVol_value_changed(value):
	AudioServer.set_bus_volume_db(1, value)
	pass # Replace with function body.


func _on_MusicVol_value_changed(value):
	AudioServer.set_bus_volume_db(2, value)
	pass # Replace with function body.


func _on_SoundFXVol_value_changed(value):
	AudioServer.set_bus_volume_db(3, value)
	pass # Replace with function body.


func _on_SoundFXVol_drag_started():
	$SoundPreview.play(0)
	pass # Replace with function body.


func _on_SoundFXVol_drag_ended(_value_changed):
	$SoundPreview.stop()
	pass # Replace with function body.


func _on_Button_pressed():
	set_visible(false)
	$"../MainScreen".set_visible(true)
	
	get_tree().root.get_node("Data").logoColor = $VBoxContainer2/LogoColor.color
	get_tree().root.get_node("Data").lineColor = $VBoxContainer2/LineColor.color
	get_tree().root.get_node("Data").lineReadyColor = $VBoxContainer2/LineReadyColor.color
	pass # Replace with function body.


func _on_Sky_toggled(button_pressed):
	get_tree().root.get_node("Data").sky = button_pressed
	pass # Replace with function body.


func _on_Button2_mouse_entered():
	$Button2/Warning.play()
	pass # Replace with function body.



func _on_Button2_mouse_exited():
	$Button2/Warning.stop()
	
	warning = 0
	updateDanger()
	pass # Replace with function body.
