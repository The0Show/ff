extends ParallaxBackground


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	
	$ParallaxLayer.motion_offset = Vector2(rand_range(-1000,1000),rand_range(-1000,1000))
	
	$ParallaxLayer/TextureRect.set_texture(get_tree().root.get_node("Data").getTexture("Sky"))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	$ParallaxLayer.motion_offset += Vector2(-0.1,0)
	
	set_visible(get_tree().root.get_node("Data").sky)
	pass
