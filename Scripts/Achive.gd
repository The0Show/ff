extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var perfection = true
var secrets = []

var ascii_letters_and_digits = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
func createGarbledString(originalString: String) -> String:
	var result = ""
	for i in range(originalString.length()):
		if(originalString[i] == " "):
			result += " "
		else:
			result += ascii_letters_and_digits[randi() % ascii_letters_and_digits.length()]
	return result

func _on_Achive_visibility_changed():
	if(!visible):
		secrets.clear()
		
	for child in $GridContainer.get_children():
		child.queue_free()
		
	if(visible):
		for status in get_tree().root.get_node("Data").achivesData.keys():
			var box = load("res://AchivementStatus.tscn").instance()
			
			box.get_node("Label").set_text(get_tree().root.get_node("Data").achivesInfo[status].name)
			box.get_node("Label2").set_text(get_tree().root.get_node("Data").achivesInfo[status].desc)
			
			if(get_tree().root.get_node("Data").achivesData[status] or (status == "perfection" and perfection)):
				box.get_node("Label3").set_text("Completed")
				box.get_node("Label3").self_modulate = Color.green
				
				if(status == "perfection" and perfection):
					get_tree().root.get_node("Data")._unlockAchivement("perfection", visible)
			else:
				if(status != "perfection"): perfection = false
				box.get_node("Label3").set_text("Not Completed")
				box.get_node("Label3").self_modulate = Color.darkred
				if(get_tree().root.get_node("Data").achivesInfo[status].secret):
					box.get_node("Label2").set_text(createGarbledString(get_tree().root.get_node("Data").achivesInfo[status].desc))
					secrets.append([box, get_tree().root.get_node("Data").achivesInfo[status].desc, 0]) 
			
			$GridContainer.add_child(box)
	
	$Button2.set_visible(perfection)
	pass # Replace with function body.


func _on_Button_pressed():
	set_visible(false)
	$"../MainScreen".set_visible(true)
	pass # Replace with function body.


func _on_Button2_pressed():
	$AudioStreamPlayer.play()
	
	$"../Tween".remove_all()
	$"../Tween".interpolate_property($"../AudioStreamPlayer", "volume_db", 0, -80, 0.5, Tween.TRANS_QUART, Tween.EASE_IN)
	$"../Tween".interpolate_property($"../CanvasLayer/ColorRect", "self_modulate", Color(1,1,1,0), Color(1,1,1,1), 0.6)
	
	$"../Tween".start()
	
	yield($"../Tween", "tween_all_completed")
	
	get_tree().change_scene("res://Scenes/DAGarden.tscn")
	pass # Replace with function body.

func _physics_process(delta):
	for box in secrets:
		if(box[2] <= 0):
			box[0].get_node("Label2").set_text(createGarbledString(box[1]))
			box[2] = randi() % 200
		else:
			box[2] -= 1
