extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(Texture) var birdTexture
export(Texture) var pigTexture

export var birdPos = Vector2(-1021,-12)
export var pigPos = Vector2(1021,-12)

export var birdScale = Vector2(1.286,1.347)
export var pigScale = Vector2(0.7,0.7)

export(Shape2D) var birdShape
export var birdShapePos = Vector2(4.5,0)
export(Shape2D) var pigShape
export var pigShapePos = Vector2(-13,8)

onready var audioStreams = [
	load("res://Assets/SoundFX/Multiplayer/GoalScored.wav"),
	load("res://Assets/SoundFX/Multiplayer/TeammateScoredGoal.wav"),
	load("res://Assets/SoundFX/Multiplayer/EnemyScoredGoal.wav")
]

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().root.get_node("Data").call_deferred("connect", "goal_scored", self, "onGoal")
	$Label2/Label3.set_text("First to " + str(get_tree().root.get_node("Data").gameConfig.goalsToWin))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ColorRect/Label3.rect_position.x = get_viewport().get_mouse_position().x - 49
	pass


func _on_Button_mouse_entered():
	$ColorRect/Label3.set_text("The")
	$ColorRect/Label3/Label2.set_text("Birds")
	pass # Replace with function body.


func _on_Button2_mouse_entered():
	$ColorRect/Label3.set_text("The")
	$ColorRect/Label3/Label2.set_text("Pigs")
	pass # Replace with function body.


func _on_Button_pressed():
	$"../../../../Bird".set_linear_velocity(Vector2(0,0))
	
	$"../../../../Bird/Sprite".set_texture(birdTexture)
	$"../../../../Bird/Sprite".set_scale(birdScale)
	
	#lmao
	$"../../../../Bird".set_position(birdPos)
	$"../../../../Bird".set_position(birdPos)
	$"../../../../Bird".set_position(birdPos)
	$"../../../../Bird".set_position(birdPos)
	$"../../../../Bird".set_position(birdPos)
	$"../../../../Bird".set_position(birdPos)
	
	$"../../../../Bird/CollisionShape2D".set_shape(birdShape)
	$"../../../../Bird/CollisionShape2D".set_position(birdShapePos)
	
	$ColorRect.set_visible(false)
	get_tree().root.get_node("Data").sendTeam(0)
	pass # Replace with function body.


func _on_Button2_pressed():
	$"../../../../Bird".set_linear_velocity(Vector2(0,0))
	
	$"../../../../Bird/Sprite".set_texture(pigTexture)
	$"../../../../Bird/Sprite".set_scale(pigScale)
	
	#lmao
	$"../../../../Bird".set_position(pigPos)
	$"../../../../Bird".set_position(pigPos)
	$"../../../../Bird".set_position(pigPos)
	$"../../../../Bird".set_position(pigPos)
	$"../../../../Bird".set_position(pigPos)
	$"../../../../Bird".set_position(pigPos)
	
	$"../../../../Bird/CollisionShape2D".set_shape(pigShape)
	$"../../../../Bird/CollisionShape2D".set_position(pigShapePos)
	
	$ColorRect.set_visible(false)
	get_tree().root.get_node("Data").sendTeam(1)
	pass # Replace with function body.

func _input(event):
	if(event is InputEventKey):
		if(event.scancode == KEY_PERIOD and event.pressed and !$ColorRect.visible):
			$ColorRect.visible = true


func _on_ColorRect_visibility_changed():
	if($ColorRect.visible):
		$ColorRect/AudioStreamPlayer.play()
		get_tree().root.get_node("Data").timer = -69420
	else:
		$ColorRect/AudioStreamPlayer.stop()
		get_tree().root.get_node("Data").timer = 0
	pass # Replace with function body.


func _on_BirdGoal_area_entered(area):
	if(get_tree().root.get_node("Data").isHost): get_tree().root.get_node("Data").sendGoalScore(0)
	$"../../../../Ball/AudioStreamPlayer2D2".play()
	pass # Replace with function body.


func _on_PigGoal_area_entered(area):
	if(get_tree().root.get_node("Data").isHost): get_tree().root.get_node("Data").sendGoalScore(1)
	$"../../../../Ball/AudioStreamPlayer2D2".play()
	pass # Replace with function body.

func onGoal(player, wasOwn, score):
	if(wasOwn):
		get_tree().root.get_node("Data").pillMsgQueue.append(get_tree().root.get_node("Data").playerData[player].name + " is very bad at sports!")
		$"../AudioStreamPlayer".set_stream(audioStreams[1])
	else:
		get_tree().root.get_node("Data").pillMsgQueue.append(get_tree().root.get_node("Data").playerData[player].name + " scored!")
		if(player == get_tree().root.get_node("Data").username):
			$"../AudioStreamPlayer".set_stream(audioStreams[0])
		else:
			$"../AudioStreamPlayer".set_stream(audioStreams[2])
	
	if($Label/AnimationPlayer.is_playing()): $Label/AnimationPlayer.stop()
	#$Label/AnimationPlayer.play("GoalScore")
	
	$Label2.set_text(str(score[0]) + " - " + str(score[1]))
	$"../AudioStreamPlayer".play()
	
	var birdMP = (score[0] + 1 >= get_tree().root.get_node("Data").gameConfig.goalsToWin)
	var pigMP = (score[1] + 1 >= get_tree().root.get_node("Data").gameConfig.goalsToWin)
	
	if(birdMP and pigMP):
		$Label2/Label3.set_text("Next Point Wins")
		$Label2/Label3.self_modulate = Color.orange
	elif(birdMP):
		$Label2/Label3.set_text("Match Point For Birds")
		$Label2/Label3.self_modulate = Color.darkgoldenrod
	elif(pigMP):
		$Label2/Label3.set_text("Match Point For Pigs")
		$Label2/Label3.self_modulate = Color.darkgreen
	else:
		$Label2/Label3.set_text("First to " + str(get_tree().root.get_node("Data").gameConfig.goalsToWin))
		$Label2/Label3.self_modulate = Color.white


func _on_Button22_pressed():
	get_tree().root.get_node("Data").leaveGame()
	get_tree().root.get_node("Data").timer = 0
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
	pass # Replace with function body.
