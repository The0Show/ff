extends Label


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var prevMousePos = Vector2(0,0)

var freeze = false

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(!freeze):
		prevMousePos = get_viewport().get_mouse_position()
		
	if(Input.is_mouse_button_pressed(1)):
		if(abs((get_viewport().get_mouse_position()-prevMousePos).x) > 100):
			set_text("Let go!")
		else:
			set_text("Pull back...")
	else:
		set_text("Hold down...")
	
	set_position(get_viewport().get_mouse_position())
	pass

func _input(event):
	if(event is InputEventMouseButton and event.button_index == 1):
		if(!event.pressed and abs((get_viewport().get_mouse_position()-prevMousePos).x) > 100):
			queue_free()
		elif(!event.pressed and abs((get_viewport().get_mouse_position()-prevMousePos).x) < 100):
			freeze = false
		else:
			freeze = true
