extends Camera2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	set_position(Vector2(3,0))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(get_tree().root.get_node("Data").freecam):
		current = true
	else:
		queue_free()
	pass

func _input(event):
	if(event is InputEventMouseMotion):
		if(Input.is_mouse_button_pressed(2)): position -= (event.relative*zoom.x)
	if(event is InputEventMouseButton):
		if(event.button_index == 5):
			zoom += Vector2(0.1,0.1)
			zoom.x = clamp(zoom.x, 0.3, 3)
			zoom.y = clamp(zoom.y, 0.3, 3)
		if(event.button_index == 4):
			zoom -= Vector2(0.1,0.1)
			zoom.x = clamp(zoom.x, 0.3, 3)
			zoom.y = clamp(zoom.y, 0.3, 3)
