extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var prevPlayerData = {}
var hostBypass = false

var DONTUPDATE = false

var prevGm = 0

var isCodeHidden = true
var hiddenCode = ""

var hatIndex = 0

var hats = {
	"default": ["None", "", "You're boring."],
	"wikipedia": ["Wikipedia", "", "Last edited by 192.168.43.23 (-1,890)"],
	"nyan": ["Nyan Cat", "", "Delicious."],
	#"undertale": ["UNDERTALE", "Created by Beckett Coburn", "Once upon a time..."],
	"error": ["ERROR", "", "Something is creating script errors, check the Problems menu for details"],
	"fedora": ["Fedora", "Created by Beckett Coburn", "What do you mean we copied Gimkit?! See?! He doesn't have a mustache! And his hat is different and everything!"],
}

func updateHat():
	if(hatIndex < 0):
		hatIndex = hats.keys().size() - 1
	
	if(hatIndex >= hats.keys().size()):
		hatIndex = 0
	get_tree().root.get_node("Data").currentHat = hats.keys()[hatIndex]
	
	$TabContainer/Outfit/Label.text = hats[get_tree().root.get_node("Data").currentHat][0]
	$TabContainer/Outfit/Label/Label2.text = hats[get_tree().root.get_node("Data").currentHat][1]
	$TabContainer/Outfit/Label/Label.text = hats[get_tree().root.get_node("Data").currentHat][2]
	
	$TabContainer/Outfit/AnimatedSprite.stop()
	$TabContainer/Outfit/AnimatedSprite.play(get_tree().root.get_node("Data").currentHat)
	
	get_tree().root.get_node("Data").updateHat()

# Called when the node enters the scene tree for the first time.
func _ready():
	get_viewport().connect("gui_focus_changed", self, "_on_focus_changed")
	
	hostBypass = true
	$TabContainer.rect_position = Vector2(48,154)
	
	for c in get_tree().root.get_node("Data").lobbyId:
		if(c != " "):
			hiddenCode += "*"
		else:
			hiddenCode += c
	$Label.text = hiddenCode
	
	get_tree().root.get_node("Data").call_deferred("connect", "player_list_change", self, "updatePlayerList")
	get_tree().root.get_node("Data").call_deferred("connect", "start_game", self, "startGame")
	
	if(get_tree().root.get_node("Data").matchResults.size() <= 0):
		$TabContainer/Results.queue_free()
	else:
		for i in get_tree().root.get_node("Data").matchResults.size():
			var node = $TabContainer/Results/SmoothScrollContainer/VBoxContainer/Base.duplicate()
			node.get_node("Label").text = str(i + 1) + ". " + get_tree().root.get_node("Data").matchResults[i][0]
			node.get_node("Label2").text = str(stepify(get_tree().root.get_node("Data").matchResults[i][1], 0.01))
			
			
			if(get_tree().root.get_node("Data").gameConfig.gameMode != 2):
				node.get_node("Label2").text += "s"
			
			$TabContainer/Results/SmoothScrollContainer/VBoxContainer.add_child(node)
			node.visible = true
	
	for child in $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer".get_children():
		child.visible = false
		
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameModeLabel".visible = true
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".visible = true
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/ResetSettingsButton2".visible = true
	
	if(gameModeSettings.size() > $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected):
		for node in gameModeSettings[$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected]:
			get_node(node).visible = true
	else:
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Seperator".visible = true
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Label".visible = true
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Label2".visible = true
	
	hatIndex = hats.keys().find(get_tree().root.get_node("Data").currentHat)
	updateHat()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$KickButton.set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Match Settings/StartButton".set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Lobby Settings/StartButton".set_disabled(!get_tree().root.get_node("Data").isHost)
	if($TabContainer.get_node_or_null("Results") != null): $"TabContainer/Results/StartButton".set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMulti".editable = get_tree().root.get_node("Data").isHost
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/PigResist".editable = get_tree().root.get_node("Data").isHost
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Gravity".editable = get_tree().root.get_node("Data").isHost
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/ResetSettingsButton2".set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/MapPack".set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GhostBirds".set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LineEdit".editable = get_tree().root.get_node("Data").isHost
	$"TabContainer/Lobby Settings/SmoothScrollContainer/VBoxContainer/IsPublicRoom".set_disabled(!get_tree().root.get_node("Data").isHost)
	$"TabContainer/Lobby Settings/SmoothScrollContainer/VBoxContainer/AccountReq".set_disabled(!get_tree().root.get_node("Data").isHost)
	
	if(!get_tree().root.get_node("Data").isHost or hostBypass):
		DONTUPDATE = true
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMulti".value = get_tree().root.get_node("Data").gameConfig.launchMulti
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/PigResist".value = get_tree().root.get_node("Data").gameConfig.pigResist
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Gravity".value = get_tree().root.get_node("Data").gameConfig.gravity
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected = get_tree().root.get_node("Data").gameConfig.gameMode
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/MapPack".selected = get_tree().root.get_node("Data").gameConfig.mapPack
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GhostBirds".pressed = get_tree().root.get_node("Data").gameConfig.ghostBirds
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LineEdit".text = str(get_tree().root.get_node("Data").gameConfig.goalsToWin)
		DONTUPDATE = false
		onSettingUpdate("a")
		
		hostBypass = false
	pass

func updatePlayerList():
	$ItemList.clear()
	
	for child in $Panel2/SmoothScrollContainer/VBoxContainer.get_children():
		if(child.name != "Base"):
			child.queue_free()
	
	for player in get_tree().root.get_node("Data").playerData:
		var node = $Panel2/SmoothScrollContainer/VBoxContainer/Base.duplicate()
		node.get_node("Label").text = get_tree().root.get_node("Data").playerData[player].name
		node.get_node("AnimatedSprite").animation = (get_tree().root.get_node("Data").playerData[player].hat)
		node.get_node("AnimatedSprite").play()
		
		if(!get_tree().root.get_node("Data").isHost or player == get_tree().root.get_node("Data").username):
			node.get_node("Kick").visible = false
			node.get_node("Promote").visible = false
		
		$Panel2/SmoothScrollContainer/VBoxContainer.add_child(node)
		node.visible = true
		yield(get_tree(), "idle_frame")
		node.name = player
		$Panel2/SmoothScrollContainer/VBoxContainer.move_child(node, $Panel2/SmoothScrollContainer/VBoxContainer/Base.get_position_in_parent())

func startGame():
	$AnimationPlayer.play("MatchStart")
	
	yield($AnimationPlayer, "animation_finished")
	
	get_tree().root.get_node("Data").timer = -2.6
	if(get_tree().root.get_node("Data").gameConfig.gameMode == 0):
		get_tree().root.get_node("Data").currentLevelPack = "Default"
		get_tree().change_scene("res://Scenes/Levels/Default/1.tscn")
	elif(get_tree().root.get_node("Data").gameConfig.gameMode == 1):
		get_tree().root.get_node("Data").currentLevelPack = "Flappy"
		get_tree().change_scene("res://Scenes/Levels/Flappy/1.tscn")
	else:
		get_tree().root.get_node("Data").currentLevelPack = "BigBall"
		get_tree().change_scene("res://Scenes/Levels/BigBall/1.tscn")
	pass


func _on_StartButton_pressed():
	get_tree().root.get_node("Data").startGame()
	pass # Replace with function body.


func _on_LeaveButton_pressed():
	get_tree().root.get_node("Data").leaveGame()
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
	pass # Replace with function body.

var gameModeSettings = [
	[
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/PigResistance",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/PigResist",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/MapPackLabel",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/MapPack",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMultiplier2",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMulti",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GhostBirds",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Grav\\",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Gravity",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Seperator",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Seperator2"
	],
	[
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GhostBirds",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Grav\\",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Gravity",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Seperator",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Seperator2"
	],
	[
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Grav\\",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Gravity",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Grav\\2",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LineEdit",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMultiplier2",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMulti",
		"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Seperator",
	]
]

func onSettingUpdate(aaaaaa):
	if(get_tree().root.get_node("Data").isHost and !DONTUPDATE):
		get_tree().root.get_node("Data").updateGameConfig({
			"launchMulti": $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMulti".value,
			"pigResist": $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/PigResist".value,
			"gravity": $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Gravity".value,
			"gameMode": $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected,
			"mapPack": $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/MapPack".selected,
			"ghostBirds": $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GhostBirds".pressed,
			"goalsToWin": $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LineEdit".text.to_int()
		})
	
	if(prevGm != $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected):
		for child in $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer".get_children():
			child.visible = false
		
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameModeLabel".visible = true
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".visible = true
		$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/ResetSettingsButton2".visible = true
		
		if(gameModeSettings.size() > $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected):
			for node in gameModeSettings[$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected]:
				get_node(node).visible = true
		else:
			$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Seperator".visible = true
			$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Label".visible = true
			$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Label2".visible = true
		
		if($"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected == 2):
			$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GhostBirds".pressed = true
		
		prevGm = $"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected

func _on_ResetSettingsButton2_pressed():
	DONTUPDATE = true
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LaunchMulti".value = get_tree().root.get_node("Data").defaultGameConfig.launchMulti
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/PigResist".value = get_tree().root.get_node("Data").defaultGameConfig.pigResist
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/Gravity".value = get_tree().root.get_node("Data").defaultGameConfig.gravity
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GameMode".selected = get_tree().root.get_node("Data").defaultGameConfig.gameMode
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/MapPack".selected = get_tree().root.get_node("Data").defaultGameConfig.mapPack
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/GhostBirds".pressed = get_tree().root.get_node("Data").defaultGameConfig.ghostBirds
	$"TabContainer/Match Settings/SmoothScrollContainer/VBoxContainer/LineEdit".text = str(get_tree().root.get_node("Data").defaultGameConfig.goalsToWin)
	DONTUPDATE = false
	onSettingUpdate("a")
	pass # Replace with function body.

var off = load("res://Assets/Sprites/outline_visibility_off_white_24dp.png")
var on = load("res://Assets/Sprites/outline_visibility_white_24dp.png")

func _on_CodeVisible_pressed():
	if(isCodeHidden):
		$Label.text = get_tree().root.get_node("Data").lobbyId
		$Label/CodeVisible.icon = on
	else:
		$Label.text = hiddenCode
		$Label/CodeVisible.icon = off
	
	isCodeHidden = !isCodeHidden
	pass # Replace with function body.


func _on_PrevHat_pressed():
	hatIndex -= 1
	updateHat()
	pass # Replace with function body.


func _on_NextHat_pressed():
	hatIndex += 1
	updateHat()
	pass # Replace with function body.

var focused = null

func _on_focus_changed(control):
	focused = control

func _on_Kick_pressed():
	get_tree().root.get_node("Data").kickPlayer(focused.get_parent().name)
	pass # Replace with function body.


func _on_Promote_pressed():
	get_tree().root.get_node("Data").promotePlayer(focused.get_parent().name)
	pass # Replace with function body.
