extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var haxxorCode = ["r","u","b","r","u","b","p","o","w","a"]
var trueCredits = ["g","i","b","e","g","g","s"]

onready var currentDb = [AudioServer.get_bus_volume_db(4), AudioServer.get_bus_volume_db(5), AudioServer.get_bus_volume_db(6)]

func setBGM1Db(value: float):
	var index = AudioServer.get_bus_index("BGM1")
	AudioServer.set_bus_volume_db(index, value)

func setBGM2Db(value: float):
	var index = AudioServer.get_bus_index("BGM2")
	AudioServer.set_bus_volume_db(index, value)

func setBGM3Db(value: float):
	var index = AudioServer.get_bus_index("BGM3")
	AudioServer.set_bus_volume_db(index, value)

# Called when the node enters the scene tree for the first time.
func _ready():
	$MainScreen/ButtonLayout/LevelSelect.get_popup().connect("id_pressed", self, "_loadLevel")
	$MainScreen/ButtonLayout/LevelPack.get_popup().connect("id_pressed", self, "changeLevelPack")
	
	$Tween.interpolate_method(self, "setBGM1Db", currentDb[0], 0, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
	$Tween.interpolate_method(self, "setBGM2Db", currentDb[1], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
	$Tween.interpolate_method(self, "setBGM3Db", currentDb[2], -80, 1, Tween.TRANS_QUART, Tween.EASE_OUT)
	
	$Tween.start()
	
	if(!$CreditsScreen.visible):
		if(get_tree().root.get_node("Data").haxxor):
			$"AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Haxxor"))
			$"AudioStreamPlayer".play()
		elif(get_tree().root.get_node("Data").speedrun):
			$"AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Competition"))
			$"AudioStreamPlayer".play()
		else:
			$"AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("MainMenu"))
				
			$"AudioStreamPlayer".play()
			
	if(get_tree().root.get_node("Data").fromDA):
		get_tree().root.get_node("Data").fromDA = false
		
		$Tween.interpolate_property($AudioStreamPlayer, "volume_db", -80, 0, 0.35, Tween.TRANS_QUART, Tween.EASE_OUT)
		$Tween.interpolate_property($CanvasLayer/ColorRect, "self_modulate", Color(1,1,1,1), Color(1,1,1,0), 0.5)
		
		$Tween.start()
	pass # Replace with function body.


func _loadLevel(num):
	get_tree().root.get_node("Data").timer = 0
	
	AudioServer.set_bus_volume_db(4,-80)
	AudioServer.set_bus_volume_db(5,-80)
	AudioServer.set_bus_volume_db(6,-80)
	var level = "Levels/"+ get_tree().root.get_node("Data").currentLevelPack + "/" + str(num)
	
	var err = get_tree().change_scene("res://Scenes/" + level + ".tscn")
	
	if(err != OK):
		OS.alert("Scene change failed (" + str(err) + ")")
		get_tree().quit()
	
func _input(event):
	if(event is InputEventKey):
		if($MainScreen.visible):
			if(haxxorCode.size() > 0 and OS.get_scancode_string(event.physical_scancode).to_lower() == haxxorCode[0]):
				haxxorCode.remove(0)
			
			if(haxxorCode.size() <= 0):
				get_tree().root.get_node("Data")._unlockAchivement("hacker")
				get_tree().root.get_node("Data").haxxor = true
				haxxorCode = [null]
				
				$AudioStreamPlayer.set_stream(get_tree().root.get_node("Data").getMusic("Haxxor"))
				$AudioStreamPlayer.play()
		elif($Achive.visible):
			if(trueCredits.size() > 0 and OS.get_scancode_string(event.physical_scancode).to_lower() == trueCredits[0]):
				trueCredits.remove(0)
			
			if(trueCredits.size() <= 0):
				get_tree().change_scene("res://Scenes/Levels/Boss/1.tscn")
				#get_tree().change_scene("res://Scenes/Credits.tscn")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$AudioStreamPlayer.set_pitch_scale(Engine.time_scale)
	
	$MainScreen/TextureRect.set_text(get_tree().root.get_node("Data").logoString)
	$CreditsScreen/VBoxContainer/TextureRect2.set_text(get_tree().root.get_node("Data").logoString)
	pass

func changeLevelPack(id):
	$MainScreen/ButtonLayout/LevelPack.set_text("Level Pack: " + $MainScreen/ButtonLayout/LevelPack.get_popup().get_item_text(id))
	get_tree().root.get_node("Data").currentLevelPack = $MainScreen/ButtonLayout/LevelPack.get_popup().get_item_text(id)
	$MainScreen.rebuildLevelSelect()


