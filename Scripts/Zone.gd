extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Zone_body_exited(body):
	body.position = Vector2(0,0)
	pass # Replace with function body.


func _on_Zone_area_exited(area):
	if(get_tree().root.get_node("Data").isHost): get_tree().root.get_node("Data").sendBallReset()
	pass # Replace with function body.
