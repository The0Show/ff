extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# further proof that this game is held together by elmers glue
var invalidGetIndexRootOnBaseNullInstanceMyAss = false

func _ready():
	if(visible):
		$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Credits"))
		$"../AudioStreamPlayer".play()
	
	invalidGetIndexRootOnBaseNullInstanceMyAss = true

func _on_CreditsScreen_visibility_changed():
	if(invalidGetIndexRootOnBaseNullInstanceMyAss):
		if(visible):
			$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Credits"))
			$"../AudioStreamPlayer".play()
		else:
			if(get_tree().root.get_node("Data").speedrun):
				$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Competition"))
				$"../AudioStreamPlayer".play()
			else:
				if(get_tree().root.get_node("Data").haxxor):
					$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("Haxxor"))
					$"../AudioStreamPlayer".play()
				else:
					$"../AudioStreamPlayer".set_stream(get_tree().root.get_node("Data").getMusic("MainMenu"))
					$"../AudioStreamPlayer".play()
	pass # Replace with function body.

func _process(_delta):
	$VBoxContainer/TextureRect2.self_modulate = get_tree().root.get_node("Data").logoColor

func _on_Button_pressed():
	set_visible(false)
	$"../MainScreen".set_visible(true)
	
	$VBoxContainer/HBoxContainer/RichTextLabel.scroll_to_line(0)
	$VBoxContainer/HBoxContainer/RichTextLabel2.scroll_to_line(0)
	pass # Replace with function body.
