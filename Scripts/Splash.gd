extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	SilentWolf.configure({
		"api_key": "1iYloHBhEK2hygtsyx9xp9xKXRKfWySc1ehXYjSA",
		"game_id": "furiousfletchlings",
		"game_version": "2.0.0",
		"log_level": 0
	})
	
	yield(SilentWolf.Auth.auto_login_player(), "sw_session_check_complete")
	
	get_tree().auto_accept_quit = false
	
	if(!OS.is_userfs_persistent()):
		OS.alert("Warning:\nThe user:// filesystem (where save data is stored) is not persistent. Save data will not be saved during this session.")
		
	$Button.visible = true
	
	yield(get_tree(), "idle_frame")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button2_pressed():
	$AudioStreamPlayer.stream.loop = false
	$AudioStreamPlayer.play()
	$Button2.queue_free()
	get_tree().root.get_node("Data")._unlockAchivement("rick")
	pass # Replace with function body.


func _on_AudioStreamPlayer_finished():
	$AudioStreamPlayer.pitch_scale += randf()/2
	$AudioStreamPlayer.play()
	pass # Replace with function body.


func _on_Button_pressed():
	$AudioStreamPlayer.stream.loop = true
	get_tree().root.get_node("Data")._unlockAchivement("bigMistake")
	get_tree().root.get_node("Data").reimportLevels()
	
	var err = get_tree().change_scene("res://Scenes/MainMenu.tscn")
	#var err = get_tree().change_scene("res://Pacer.tscn")
	#get_tree().root.get_node("Data").currentEditorState = 1
	#var err = get_tree().change_scene("res://Scenes/Levels/Boss/1.tscn")
	
	if(err != OK):
		OS.alert("Scene change failed (" + str(err) + ")")
		get_tree().quit()
	pass # Replace with function body.


func _on_LineEdit_text_entered(new_text):
	if(new_text == "jacob is very very bad"):
		$Button.emit_signal("pressed")
	pass # Replace with function body.
