extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum State {
	IDLE,
	ACTION
}

export var pigs = 0
export(State) var state = 0
export var pipeCount = 1
var stopPipes = false

export var ending = false

export var prevTimeScale = Engine.time_scale
var file = File.new()

var levelNum = "0"

func _ready():
	levelNum = get_tree().get_current_scene().get_filename().split("/")[-1].replace(".tscn", "")
	
	if(name == "Pacer"):
		$AudioStreamPlayer.connect("finished", self, "onPacerEnd")
		
	if(name == "BigBall"):
		get_tree().root.get_node("Data").connect("end_game", self, "endGame")
	
	$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.connect("pressed", self, "_on_NextLevel_pressed")
	
	if(name == "Flappy"):
		$AnimationPlayer.play("New Anim")
	
		for child in $Level.get_children():
			child.position.y = rand_range(0, 353)
			child.get_node("Label").text = str(pipeCount)
			pipeCount += 1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(pigs < 0):
		get_tree().root.get_node("Data")._unlockAchivement("pig-1")
	
	if(get_tree().root.get_node("Data").currentEditorState == 2):
		$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.set_text("Editor")
	else:
		if(state == State.IDLE):
			$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.visible = Input.is_key_pressed(KEY_ENTER)
			$CameraTarget/Camera2D/CanvasLayer/Control/Label.visible = !Input.is_key_pressed(KEY_ENTER)
			
			$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.set_text("Main Menu")
			pass
		elif(state == State.ACTION):
			$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.visible = true
			$CameraTarget/Camera2D/CanvasLayer/Control/Label.visible = false
			if(pigs <= 0):
				$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.set_text("Next Level")
			else:
				$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.set_text("Retry Level")
	
	$Bird/BGM1.set_pitch_scale(Engine.time_scale)
	$Bird/BGM2.set_pitch_scale(Engine.time_scale)
	$Bird/BGM3.set_pitch_scale(Engine.time_scale)
	
	$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.set_disabled(get_tree().root.get_node("Data").blockLevelSwitching)
	
	if(name == "Flappy" and !stopPipes and get_tree().root.get_node("Data").timer >= 0):
		for child in $Level.get_children():
			child.position.x -= (500*_delta)+(get_tree().root.get_node("Data").timer/20)
			$AnimationPlayer.playback_speed = 1+(get_tree().root.get_node("Data").timer/60)
			
			if(child.position.x <= -2600):
				child.position.x = 1731
				print(1731+(get_tree().root.get_node("Data").timer*2))
				child.position.y = rand_range(0, 353)
				child.get_node("Label").text = str(pipeCount)
				pipeCount += 1
	pass

func _input(_event):
	if(Input.is_action_just_pressed("free_camera")):
		$CameraTarget/Camera2D.limit_left = -10000000
		$CameraTarget/Camera2D.limit_top = -10000000
		$CameraTarget/Camera2D.limit_right = 10000000
		$CameraTarget/Camera2D.limit_bottom = 10000000

func _on_NextLevel_pressed():
	ending = !file.file_exists("res://Scenes/Levels/" + get_tree().root.get_node("Data").currentLevelPack + "/" + str(levelNum.to_int() + 1) + ".tscn")
	
	if(get_tree().root.get_node("Data").currentEditorState == 2):
		get_tree().root.get_node("Data").currentEditorState = 1
		get_tree().reload_current_scene()
	else:
		get_tree().root.get_node("Data").connect("end_game", self, "endGame")
		
		Engine.time_scale = prevTimeScale
		
		AudioServer.set_bus_volume_db(4,-80)
		AudioServer.set_bus_volume_db(5,-80)
		AudioServer.set_bus_volume_db(6,-80)
		
		if(state == State.IDLE and name != "Flappy"):
			if(get_tree().root.get_node("Data").lobbyId != ""):
				get_tree().root.get_node("Data").leaveGame()
				get_tree().root.get_node("Data").timer = 0
				get_tree().change_scene("res://Scenes/MainMenu.tscn")
			else:
				get_tree().root.get_node("Data").timer = 0
				var err = get_tree().change_scene("res://Scenes/MainMenu.tscn")
			
				if(err != OK):
					OS.alert("Scene change failed (" + str(err) + ")")
					get_tree().quit()
		elif(state == State.ACTION or name == "Flappy"):
			if(pigs <= 0):
				if(ending):
					if(get_tree().root.get_node("Data").lobbyId != ""):
						$CameraTarget/Camera2D/CanvasLayer/Control/SpeedrunTimer.freeze = true
						$CameraTarget/Camera2D/CanvasLayer/Control/Panel4.set_visible(true)
						get_tree().root.get_node("Data").sendTime()
					else:
						get_tree().root.get_node("Data")._unlockAchivement("firstTime")
						
						var file = File.new()
						if(!file.file_exists("user://lvlsel")):
							file.open("user://lvlsel", File.WRITE)
							file.store_double(-69)
							
							file.close()
							
							AudioServer.set_bus_mute(1,true)
							OS.alert("You've unlocked the level selector and speedrun timer!")
							AudioServer.set_bus_mute(1,false)
						elif(get_tree().root.get_node("Data").speedrun):
							$CameraTarget/Camera2D/CanvasLayer/Control/SpeedrunTimer.freeze = true
							file.open("user://lvlsel", File.READ_WRITE)
							
							var pb = file.get_double()
							
							if(((pb > get_tree().root.get_node("Data").timer) or (pb == -69)) and get_tree().root.get_node("Data").currentLevelPack == "Default"):
								file.close()
								
								file.open("user://lvlsel", File.WRITE)
								
								file.store_double(get_tree().root.get_node("Data").timer)
								get_tree().root.get_node("Data").newpb = true
		#					elif():
		#						file.close()
		#
		#						file.open("user://lvlsel", File.WRITE)
		#
		#						file.store_double(get_tree().root.get_node("Data").timer)
		#						get_tree().root.get_node("Data").newpb = true
								
							file.close()
							
							get_tree().root.get_node("Data")._unlockAchivement("goodTime")
							
							if(get_tree().root.get_node("Data").timer <= 60):
								get_tree().root.get_node("Data")._unlockAchivement("itsNotThatBad")
							
							if(get_tree().root.get_node("Data").timer <= 30):
								get_tree().root.get_node("Data")._unlockAchivement("sub30")
								
							if(get_tree().root.get_node("Data").timer <= 15):
								get_tree().root.get_node("Data")._unlockAchivement("creative")
							
							if(get_tree().root.get_node("Data").timer <= 10):
								get_tree().root.get_node("Data")._unlockAchivement("how")
								
							if(SilentWolf.Auth.logged_in_player != null):
								print(get_tree().root.get_node("Data").currentLevelPack + "@" + get_tree().root.get_node("Data").levelPackKeys[get_tree().root.get_node("Data").currentLevelPack])
								$CameraTarget/Camera2D/CanvasLayer/Control/Panel4/Label.set_text("Submitting time ...")
								$CameraTarget/Camera2D/CanvasLayer/Control/Panel4.set_visible(true)
								yield(SilentWolf.Scores.persist_score(SilentWolf.Auth.logged_in_player, int((65535*1000)-get_tree().root.get_node("Data").timer*1000), get_tree().root.get_node("Data").currentLevelPack + "@" + get_tree().root.get_node("Data").levelPackKeys[get_tree().root.get_node("Data").currentLevelPack]), "sw_score_posted")
						
						var scene = load("res://Scenes/MainMenu.tscn").instance()
						
						if(get_tree().root.get_node("Data").speedrun):
							scene.get_node_or_null("MainScreen").set_visible(true)
							scene.get_node_or_null("MainScreen/ColorRect").set_visible(true)
						else:
							scene.get_node_or_null("MainScreen").set_visible(false)
							scene.get_node_or_null("CreditsScreen").set_visible(true)
						
						get_tree().root.add_child(scene)
						
						get_tree().set_current_scene(scene)
						
						queue_free()
				else:
					var err = get_tree().change_scene("res://Scenes/Levels/" + get_tree().root.get_node("Data").currentLevelPack + "/" + str(levelNum.to_int() + 1) + ".tscn")
		
					if(err != OK):
						OS.alert("Scene change failed (" + str(err) + ")")
						get_tree().quit()
			else:
				if(name == "Flappy"):
					get_tree().root.get_node("Data").leaveGame()
					get_tree().root.get_node("Data").timer = 0
					get_tree().change_scene("res://Scenes/MainMenu.tscn")
				else:
					var oldname = levelNum
					
					var scene = load(get_tree().get_current_scene().get_filename()).instance()
					
					set_name("temp")
					
					scene.set_name(oldname)
						
					get_tree().root.add_child(scene)
						
					get_tree().set_current_scene(scene)
						
					queue_free()
	pass # Replace with function body.

func endGame():
	$CameraTarget/Camera2D/CanvasLayer/Control/Panel4.set_visible(true)
	
	$CameraTarget/Camera2D/CanvasLayer/Control/Panel4/Label3.text = get_tree().root.get_node("Data").winningPlayer.playerName
	$CameraTarget/Camera2D/CanvasLayer/Control/Panel4/Label4.text = str(stepify(get_tree().root.get_node("Data").winningPlayer.time, 0.01)) + "s"
	
	$CameraTarget/Camera2D/CanvasLayer/Control/AnimationPlayer.play("GameOver")
	
	yield($CameraTarget/Camera2D/CanvasLayer/Control/AnimationPlayer, "animation_finished")
	get_tree().root.get_node("Data").timer = 0
	
	get_tree().change_scene("res://Scenes/Lobby.tscn")


func _on_Pig_body_entered(body):
	if(body.name == "Bird"):
		stopPipes = true
		pigs = 0
		$CameraTarget/Camera2D/CanvasLayer/Control/NextLevel.emit_signal("pressed")
		$AnimationPlayer.stop(false)
	pass # Replace with function body.

func onPacerEnd():
	JavaScript.eval('[...Array(2**32-1)].map(_=>Math.ceil(Math.random()*111))')
