extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var dir = Directory.new()
var file = File.new()
var levelsInPack = 0
var selectedPack = ""
var selectedLevel = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$VBoxContainer2/RemoveLevel.disabled = selectedLevel == null
	$VBoxContainer2/EditLevel.disabled = selectedLevel == null
	pass

var stop = false

func _on_LevelEditor_visibility_changed():
	$VBoxContainer/LevelPacks/ItemList.clear()
	
	selectedLevel = null
	$VBoxContainer.visible = true
	$VBoxContainer2.visible = false
	
	if(visible):
		$"../MainScreen/ButtonLayout/Speedrun".pressed = false
	
		if(!dir.dir_exists("user://WIPLevels/")):
			dir.make_dir("user://WIPLevels/")
		
		if dir.open("user://WIPLevels/") == OK:
			dir.list_dir_begin(true, true)
			var file_name = dir.get_next()
			while file_name != "":
				if dir.current_is_dir():
					$VBoxContainer/LevelPacks/ItemList.add_item(file_name)
				file_name = dir.get_next()
	pass # Replace with function body.

func updateLevelList():
	levelsInPack = 0
	$VBoxContainer2/Levels/LevelItemList.clear()
	if dir.open("user://WIPLevels/" + selectedPack + "/") == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir() and file_name.ends_with(".tscn"):
				$VBoxContainer2/Levels/LevelItemList.add_item("Level " + str(int(file_name.replace(".tscn", ""))))
				levelsInPack += 1
			file_name = dir.get_next()

func _on_ItemList_item_selected(index):
	selectedPack = $VBoxContainer/LevelPacks/ItemList.get_item_text(index)
	
	updateLevelList()
	
	$VBoxContainer.visible = false
	$VBoxContainer2.visible = true
	pass # Replace with function body.


func _on_Button_pressed():
	visible = false
	$"../MainScreen".visible = true
	pass # Replace with function body.


func _on_AddLevel_pressed():
	dir.copy("res://Scenes/CustomLevelBase.tscn", "user://WIPLevels/" + selectedPack + "/" + str(levelsInPack+1) + ".tscn")
	updateLevelList()
	pass # Replace with function body.


func _on_LevelItemList_item_selected(index):
	selectedLevel = index+1
	pass # Replace with function body.


func _on_RemoveLevel_pressed():
	dir.remove("user://WIPLevels/" + selectedPack + "/" + str(selectedLevel) + ".tscn")
	
	levelsInPack = 0
	if dir.open("user://WIPLevels/" + selectedPack + "/") == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir():
				levelsInPack += 1
				dir.rename(file_name, "user://WIPLevels/" + selectedPack + "/" + str(levelsInPack) + ".tscn")
			file_name = dir.get_next()
	
	get_tree().reload_current_scene()
	pass # Replace with function body.
	

func _on_EditLevel_pressed():
	get_tree().root.get_node("Data").currentEditorState = 1
	get_tree().change_scene("user://WIPLevels/" + selectedPack + "/" + str(selectedLevel) + ".tscn")
	pass # Replace with function body.

func _on_DeletePack_pressed():
	#var confirmation = JavaScript.eval('confirm("Woah there, deleting this pack will remove it from your device *forever*! (a long time) Do you still want to do this?")')
	if(true):
		var file = File.new()
		if dir.open("user://WIPLevels/" + selectedPack + "/") == OK:
			dir.list_dir_begin(true, true)
			var file_name = dir.get_next()
			while file_name != "":
				if !dir.current_is_dir():
					dir.remove("user://WIPLevels/" + selectedPack + "/" + file_name)
					yield(get_tree().create_timer(0.1), "timeout")
				file_name = dir.get_next()
		dir.remove("user://WIPLevels/" + selectedPack)
		yield(get_tree().create_timer(0.1), "timeout")
		get_tree().reload_current_scene()
	pass # Replace with function body.


func _on_NewPack_pressed():
	var packname = JavaScript.eval('prompt("Name your pack (a-z and spaces only)")')
	if(packname == "" or packname == null):
		pass
	else:
		var regex = RegEx.new()
		regex.compile("^[a-zA-Z0-9- ]+$")
		var result = regex.search(packname)
		if(result == null or packname.replace(" ", "") == ""):
			OS.alert("Invalid name, poopyhead")
		else:
			selectedPack = packname
			dir.make_dir("user://WIPLevels/" + selectedPack)
			
			$VBoxContainer/LevelPacks/ItemList.clear()
		
			if dir.open("user://WIPLevels/") == OK:
				dir.list_dir_begin(true, true)
				var file_name = dir.get_next()
				while file_name != "":
					if dir.current_is_dir():
						$VBoxContainer/LevelPacks/ItemList.add_item(file_name)
					file_name = dir.get_next()
	pass # Replace with function body.

func _on_BuildPack_pressed():
	var packer = PCKPacker.new()
	packer.pck_start("user://WIPLevels/" + selectedPack + "/" + selectedPack + ".fflvlpck")
	if dir.open("user://WIPLevels/" + selectedPack + "/") == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir() and file_name.ends_with(".tscn"):
				packer.add_file("res://Scenes/Levels/" + selectedPack + "/" + file_name, "user://WIPLevels/" + selectedPack + "/" + file_name)
			file_name = dir.get_next()
	packer.flush()
	
	file.open("user://WIPLevels/" + selectedPack + "/" + selectedPack + ".fflvlpck", File.READ)
	JavaScript.download_buffer(file.get_buffer(file.get_len()), selectedPack + ".fflvlpck")
	file.close()
	
	dir.remove("user://WIPLevels/" + selectedPack + "/" + selectedPack + ".fflvlpck")
	pass # Replace with function body.
