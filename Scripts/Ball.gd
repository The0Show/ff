extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var velocity = Vector2(0,0)
var rotationn = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	position = get_tree().root.get_node("Data").ballPos
	rotate(get_tree().root.get_node("Data").ballRot)
	pass


func _on_Ball_body_entered(node):
	$AudioStreamPlayer2D.play()
	
	if(node is RigidBody2D):
		#velocity += node.linear_velocity/60
		#rotationn = node.angular_velocity/100
		
		get_tree().root.get_node("Data").sendBallHit(node.linear_velocity, node.angular_velocity)
	else:
		#velocity.x *= -abs(clamp(randf(),0.2,1))
		#velocity.y *= -abs(clamp(randf(),0.2,1))
		
		if(get_tree().root.get_node("Data").isHost): get_tree().root.get_node("Data").sendWallHit()
	pass # Replace with function body.
