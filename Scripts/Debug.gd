extends RichTextLabel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var targetFps = OS.get_screen_refresh_rate()

export var lines = []
export var birdVel = Vector2(0,0)
export var pigVel = Vector2(0,0)
var f2pressed = false

func _ready():
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	
	visible = OS.is_debug_build()
	
	if (targetFps < 0):
		targetFps = 60.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	set_visible(get_tree().root.get_node("Data").openDebug)
	
	text = ""
	
	lines = [
		"Godot Engine v" + Engine.get_version_info().string + " on " + OS.get_name(),
		"user:// is NOT persistent",
		str(Performance.get_monitor(Performance.TIME_FPS)) + " FPS",
		str(Performance.get_monitor(Performance.RENDER_DRAW_CALLS_IN_FRAME) + Performance.get_monitor(Performance.RENDER_2D_DRAW_CALLS_IN_FRAME)) + " draw calls", # combining 3d and 2d draw calls
		str(Performance.get_monitor(Performance.OBJECT_COUNT)) + " instanced objects",
		"current_scene: " + get_tree().get_current_scene().get_filename(),
		get_tree().root.get_node("Data").username
	]
	
	if(OS.is_userfs_persistent()):
		lines[1] = "user:// is persistent"
	
	for line in lines:
		text += str(line)
		if(lines.find(line)+1 != lines.size()):
			text += "\n"
	pass
