extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var resistance = Vector2(6500,6500)

var prevVel = Vector2(0,0)

var highestVel = Vector2(0,0)

var endMyMisery = false

export var canBeKilled = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _integrate_forces(state):
	var force = state.get_linear_velocity() / state.get_inverse_mass() / state.get_step()
	
	if((abs(force.x) > (resistance.x*get_tree().root.get_node("Data").pigResist) or abs(force.y) > (resistance.y*get_tree().root.get_node("Data").pigResist)) and !endMyMisery and canBeKilled):
		endMyMisery = true
		
		$"../../CameraTarget/Camera2D/CanvasLayer/Control/Debug".pigVel = force
		
		$"../..".pigs -= 1
		
		var explodeSound = AudioStreamPlayer.new()
		
		explodeSound.bus = "New Bus"
		explodeSound.set_stream(load("res://Assets/SoundFX/Explode.wav"))
		explodeSound.connect("finished", explodeSound, "queue_free")
		
		get_tree().root.add_child(explodeSound)
		
		explodeSound.play()
		
		queue_free()
